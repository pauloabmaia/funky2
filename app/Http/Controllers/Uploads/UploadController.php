<?php

namespace App\Http\Controllers\Uploads;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UploadController extends Controller {
    public function musica(Request $request){
        $cookienick = $_COOKIE['nick'];
        $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $cookienick)->get();
        foreach ($users as $user) {
            $idArtista = $user -> id;
        }
        $nome = request('musicname');
        $descricao = request('musicdescrption');
        if ($descricao == null){
            $descricao = "";
        }
        $imagem = $request->file('musicphoto');
        $musics = DB::table('Musica')->get();
        foreach ($musics as $music) {
            $dbnome = $music -> nome;
            if ($dbnome == $nome) {
                echo "Já existe essa música";
                return view('upload');
            }
        }
        //$request->file('musicphoto')->storeAs('public/fotomusica', $nome.".jpg");
        $s3 = \Storage::disk('s3');
        if ($request->hasFile('music')){
            //$request->file('music')->storeAs('public/musica', $nome.".mp3");
            $music = $request->file('music');
            $teste = $request->file('music')->getClientOriginalName();
            $musicname = $nome.".".$music->getClientOriginalExtension();
	    $ext_music=$music->getClientOriginalExtension();
	    if ($ext_music != "mp3" and $ext_music != "flac" and $ext_music != "ogg" and $ext_music != "wav" and $ext_music != "aiff" and $ext_music != "aac" and $ext_music != "aa" and $ext_music != "wma" and $ext_music != "pcm" and $ext_music != "ogg" and $ext_music != "alac"){
	    	    echo "Formato de música inválido";
                    return view('upload');
	
	    }
            $filePath = '/musics/' . $musicname;
            $s3->put($filePath, file_get_contents($music), 'public');

            $image = $request->file('musicphoto');
            $imagename = $nome.".".$image->getClientOriginalExtension();
	$ext = $image->getClientOriginalExtension();
	    if ($ext != "jpg" and $ext != "psd" and $ext != "tiff" and $ext != "jpeg" and $ext != "png" and $ext != "gif" and $ext != "raw"){
                 echo $ext."ola";
		 echo "Formato de imagem inválido";
                 return view('upload');

            }

            $filePath = '/musics/images/' . $imagename;
            $s3->put($filePath, file_get_contents($image), 'public');

            $musicabd = DB::table('Musica')->insert(['nome' => $musicname, 'image' => $imagename, 'descricao' => $descricao, 'idArtista' => $idArtista, 'nomemusica' => $nome]);
        }

?>
<script>
    window.location.href = "index";
</script>
<?php
    }
    public function video(Request $request) {
        $cookienick = $_COOKIE['nick'];
        $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $cookienick)->get();
        foreach ($users as $user) {
            $idArtista = $user -> id;
        }
        $nome = request('videoname');
        $descricao = request('videodescrption');
        $video = $request->file('video');

        if($nome&&$video){
            $videos = DB::table('Video')->get();
            foreach ($videos as $video) {
                $dbnome = $video -> nome;
                if ($dbnome == $nome) {
                    echo "Já existe esse video";
                    return view('upload');
                }
            }
            //$request->video->storeAs('public/video', $nome.".mp4");

            $s3 = \Storage::disk('s3');
            if ($request->hasFile('video')){
                //$request->file('music')->storeAs('public/musica', $nome.".mp3");
                $video = $request->file('video');
                $videoname = $nome.".".$video->getClientOriginalExtension();
		$ext=$video->getClientOriginalExtension();
                if ($ext != "avi" and $ext != "asf" and $ext != "mov" and $ext != "avchd" and $ext != "flv" and $ext != "mpg" and $ext != "mp4" and $ext != "wmv" and $ext != "divx"){
                    echo "Formato de vídeo inválido";
                    return view('upload');

                }

                $filePath = '/videos/' . $videoname;
                $s3->put($filePath, file_get_contents($video), 'public');
                $video = DB::table('Video')->insert(['nome'=>$videoname,'descricao'=>$descricao,'video'=>$nome,'idArtista'=>$idArtista]);
            }

?>
<script>
    window.location.href = "index";
</script>
<?php
        }
    }
}
