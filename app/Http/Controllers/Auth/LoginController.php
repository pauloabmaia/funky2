<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller {

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	//protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	/*public function __construct() {
		$this->middleware('guest', ['except' => 'logout']);
	}*/
	
	public function entrar() {
		$nick = request('username');
		$pass = request('password');
		$password = crypt($pass, 'E1F53135E559C253');
		if($nick && $password){
			$users = DB::table('UtilizadoresRegistados')->select('nick', 'email', 'password')->where('nick', $nick)->orWhere('email', $nick)->get();
			if (!$users->isEmpty()){
				foreach ($users as $user) {
					$dbnick = $user -> nick;
					$dbemail = $user -> email;
					$dbpassword = $user -> password;
					if(($nick==$dbnick || $nick==$dbemail) && $password == $dbpassword){
						setcookie('nick', $dbnick, time() + (86400 * 30));
						setcookie('login', 'true', time() + (86400 * 30));
						?>
						<script>
							window.location.href = 'index';
						</script>
						<?php
					} else{
						echo "Nick ou palavra-passe errados";
                        			return view('entrar');
					}
				}
			}
			 else{
				echo "Nick ou palavra-passe errados";
                		return view('entrar');
			}
		} else{
			echo "Introduza o nick e password";
            		return view('entrar');
			}
	}

	public function logout(){
		if (isset($_SERVER['HTTP_COOKIE'])){
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach($cookies as $cookie){
				$mainCookies = explode('=', $cookie);
				$name = trim($mainCookies[0]);
				setcookie($name, '', time()-1000);
				setcookie($name, '', time()-1000, '/');
			}
		}
		setcookie('login', 'false');
		?>
		<script>
			window.location.href = "index";
		</script>
		<?php
	}
}
