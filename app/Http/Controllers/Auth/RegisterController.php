<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;

class RegisterController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function registar(Request $request){
        $nome = request('firstname');
        $nick = request('username');
        $password = request('passwd');
        $repeatpassword = request('repeatpasswd');
        $email = request('email');
        $genero = request('gender');
        $data_nascimento = request('dateofbirth');
        $pais = request('country');

        if($nome&&$nick&&$password&&$email&&$genero&&$data_nascimento&&$pais&&$request->file('avatar')){
            if($password == $repeatpassword){
                if(strlen($password)>16 || strlen($password)<4){
                    echo "<p>Palavra-passe tem que ter entre 4 e 16 caracteres!</p>";
                    return view('registo');
                }
                else{
                    if(strlen($nick)>32){
                        echo "Tamanho do nickname demasiado longo";
                        return view('registo');

                    }
                    else{
                        if(strtotime($data_nascimento)> time() or strtotime($data_nascimento)< strtotime("1900-12-31")){
                            ?>
                            <script>
                                /*var popup = document.getElementById("myPopup");
                                popup.classList.toggle("show");*/
                            </script>
                            <?php
                            echo "Data de Nascimento inválida";
                            return view('registo');
                        } else{
                            $users = DB::table('UtilizadoresRegistados')->get();
                            foreach ($users as $user) {
                                $dbnick = $user -> nick;
                                if ($dbnick == $nick){
                                    echo "Já existe esse utilizador";
                                    return view('registo');
                                }
                            }
                            $image = $request->file('avatar');
                            $imagename = $nick.".".$image->getClientOriginalExtension();
			    $ext = $image->getClientOriginalExtension();
			    if ($ext != "jpg" and $ext != "psd" and $ext != "tiff" and $ext != "jpeg" and $ext != "png" and $ext != "gif" and $ext != "raw"){
                 		echo "Formato de imagem inválido";
				echo $ext."ola";
                 		return view('registo');
            		    }

                            $s3 = \Storage::disk('s3');
                            $filePath = '/profiles/' . $imagename;
                            $s3->put($filePath, file_get_contents($image), 'public');

                            //$request->avatar->storeAs('public/users', $imagename);
                            $pass = crypt($password, 'E1F53135E559C253');

                            $user = DB::table('UtilizadoresRegistados')->insert( ['nick' => $nick, 'nome' => $nome, 'genero'=> $genero, 'dataNascimento' => $data_nascimento, 'email' => $email, 'password' => $pass, 'image' => $imagename]);
                            setcookie('nick', $nick, time() + (86400 * 30));
                            setcookie('login', 'true', time() + (86400 * 30));
                        ?>
                        <script>
                            window.location.href = "index";
                        </script>
                        <?php
                        }
                    }
                }
            }
            else{
                echo "As palavras-passe não coincidem.";
                ?>
                <script>
                    window.history.back();
                </script>
                <?php
            }
        }
        else{
            echo "Por favor preencha <b>todos</b> os campos!";
            return view('registo');
        }
    }
}
