<?php

namespace App\Http\Controllers\Mensagens;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MensagensController extends Controller {
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $redirectTo = '/musi?id='; //path da musica
    
    public function mensagem(Request $request) {
        //fazer as cenas com a mensagem e a base de dados
        $idMusica = request('idmusica');
		$descricao = request('mensagem_a_submeter');
		$cookienick = $_COOKIE['nick'];
        $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $cookienick)->get();
        foreach ($users as $user) {
            $idArtista = $user -> id;
        }
		$mensagem = DB::table('Mensagens')->insert( ['idmusica' => $idMusica, 'uid' => $idArtista, 'descricao'=> $descricao]);
        #return view('musi?id='.$idMusica); //path da musica
        echo "<script>window.location.href = 'musi?id=".$idMusica."';</script>";
    }
}?>
