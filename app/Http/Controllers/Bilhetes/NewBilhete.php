<?php

namespace App\Http\Controllers\Bilhetes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class NewBilhete extends Controller {

	public function newBilhete(Request $request) {
		$lugares = request('bilhetes');
		$id_espetaculo = request('id_espetaculo');
		$id_comprador = request('id_comprador');
		$cenas = explode(',', $lugares);
		if ($cenas== ''){
			echo "Selecione algum lugar";
			return Redirect::back();
		} else{
			foreach ($cenas as $key) {
				$espetaculo = DB::table('Espetaculos')->where('id', $id_espetaculo)->select('*')->get();
				foreach ($espetaculo as $chave) {
					$preco = $chave -> preco;
					$qttbilhetes = $chave -> qtdBilhetes;
					$dbsala = $chave -> tamanhoSala;
  					$linhascolunas = explode('x', $dbsala);
  					$linhas = (int)$linhascolunas[0];
  					$colunas = (int)$linhascolunas[1];
				}
				if($linhas == 0 or $colunas == 0){
                                	$quantidade = (int)$lugares;
                                } else{
                                	$quantidade = 1;
                                }
				if($qttbilhetes<$quantidade){
                                	echo "O numero de bilhetes pedido é maior do que os disponíveis";
                                	return view("compra");
				} else{	
					$quantidade = (int)$qttbilhetes-$quantidade;
					$user = DB::table('Bilhetes')->insert( ['id_comprador' => $id_comprador, 'id_espetaculo' => $id_espetaculo, 'preco'=> $preco, 'lugar' => $key]);
					DB::table('Espetaculos')->where('id', $id_espetaculo)->update(['qtdBilhetes' => $quantidade]);
				}
				}

    echo "<script>window.location.href = 'meusbilhetes?id=".$id_comprador."';</script>";
		}
	}
}
