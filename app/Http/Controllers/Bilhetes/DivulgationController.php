<?php

namespace App\Http\Controllers\Bilhetes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DivulgationController extends Controller {

	public function divulgar(Request $request) {
		$nomeArtista = $_COOKIE['nick'];
		$artistas = DB::table('UtilizadoresRegistados')->where('nick', $nomeArtista)->get();
		foreach ($artistas as $artista) {
			$idArtista = $artista -> id;
		}
		$nome = request('eventname');
		$descricao = request('eventdescrption');
		$datahorainicio = request('initeventdate');
		$datahorafim = request('endeventdate');
		$local = request('local');
		$qtdbilhetes = request('tickets');
		$sala = request('sala');
		if ($sala == 'personalizada') {
			$linhas = request('linhas');
			$colunas = request('colunas');
			if($linhas <= 23 and $linhas >= 0 and $colunas <= 23 and $colunas >= 0) {
				$sala = $linhas."x".$colunas;
				$qtdbilhetes = (int)$linhas * (int)$colunas;
			} else {
				echo "A sala só pode ter até 23 linhas ou colunas";
				return view('divulgacao');
			}
		} else{ 
			if($sala == '0x0'){
				$qtdbilhetes = request('tickets');
			} else{
				$linhascolunas = explode('x', $sala);
  				$linhas = (int)$linhascolunas[0];
  				$colunas = (int)$linhascolunas[1];
				$qtdbilhetes = (int)$linhas * (int)$colunas;
			}
		}
		$preco = request('preco');
		if ($preco == 'free') {
			$precofinal = 'free';
		} else{
			$precofinal = request('precobilhete');
		}
		if ($request->file('eventphoto')&&$nome&&$descricao&&$datahorainicio&&$datahorafim&&$local&&$qtdbilhetes&&$precofinal){
			if(strtotime($datahorainicio)<strtotime($datahorafim)){
				$image = $request->file('eventphoto');
				$imagename = $nome.".".$image->getClientOriginalExtension();
				$s3 = \Storage::disk('s3');
				$filePath = '/shows/' . $imagename;
				$s3->put($filePath, file_get_contents($image), 'public');
				if ($precofinal == 'free') {
					$precofinal = 0;
				}
				$user = DB::table('Espetaculos')->insert( ['descricao' => $descricao, 'nome' => $nome, 'local' => $local,
				'datainicio' => $datahorainicio, 'datafim' => $datahorafim, 'qtdBilhetes' => $qtdbilhetes, 'preco' => $precofinal,
				'imagem' => $imagename, 'idArtista' => $idArtista, 'tamanhoSala' => $sala]);
			} else {
				echo "Datas invalidas";
				return view('divulgacao');
			}
		}
		else{
			echo "Por favor preencha <b>todos</b> os campos!";
			return view('divulgacao');
		}
		?>
		<script type="text/javascript">
			window.location.href = "index";
		</script>
		<?php
	}
}
