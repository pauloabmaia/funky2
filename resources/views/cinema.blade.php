<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png"  href="Imagem1.png" />
        <title>FunkyNotes</title>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Theme CSS -->
        <link href="css/freelancer.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/portfolio-item.css" rel="stylesheet">
    </head>

    <body>
      <!-- Navigation -->
            <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="btn btn-default1 navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">FunkyNotesNav</span>
                            <span class="glyphicon glyphicon-menu-down"></span>
                        </button>
                        <a class="navbar-brand" href="index">FunkyNotes</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <!-- dropdown -->
                      <ul class="navbar-form navbar-left">
                        <div class="dropdown">
                          <button class="btn btn-default1 dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li class="opcao"><a href="espetaculos">Espectáculos</a></li>
                            <li class="opcao"><a href="musicas">Música</a></li>
                            <li class="opcao"><a href="videos">Vídeos</a></li>
                          </ul>
                        </div>
                      </ul>

                      <div id="terminariniciar">
                          <ul class="nav navbar-nav navbar-right nav-main" id="reglogbar">
                                <?php
                                if (isset($_COOKIE['login']) and $_COOKIE['login'] == 'true'){
                                ?>
                                  <ul style="margin-top:7px; " class="navbar-nav navbar-right" id="reglogbar">
                                    <li>
                                      <div class="dropdown">
                                        <button type="button" class="btn btn-default1 dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                          <li class="opcao"><a href="divulgacao">Adicionar Espetáculos</a></li>
                                          <li class="opcao"><a href="upload">Upload Musicas/Videos</a></li>
                                        </ul>
                                      </div>
                                    </li>
                                  <li>
                                    <button type="button" class="btn btn-link dropdown-toggle " data-toggle="dropdown">
                                        <?php
                                    $nick = $_COOKIE['nick'];
                                    $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                    foreach ($users as $user) {
                                        $imagem = $user -> image;
                                    }
                                    echo"<span aria-hidden='true'><img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$imagem."' class='util img-circle' style='margin-top:-7px; margin-left:10px; '></span>";
                                            ?>
                                        </button>

                                        <ul class="dropdown-menu" role="menu">
                                            <?php
                                    $nick = $_COOKIE['nick'];
                                    $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                    foreach ($users as $user) {
                                        $dbid = $user -> id;
                                    }
                                    echo "<li><a href='artista?id=".$dbid."onClick='$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();''>Perfil</a></li>";
                                            ?>
                                            <li><a href="logout">Terminar Sessão</a></li>
                                        </ul>

                                </li>
                                <?php
                                }
                                else{
                                ?>
                                <li><a href='registo'>Registar</a></li>";
                                <li><a href='entrar'>Entrar</a></li>
                                <?php
                                }
                                ?>
                                  </ul>
                            </ul>
                        </div>

                        <form class="navbar-form navbar-left" action="search" method="get" role="form">
                            <!-- <div id="custom-search-input"> -->
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Pesquisa" name="pesquisa"/>
                                    <div class="input-group-btn">
                                        <button class="btn btn-default1" type="button">
                                            <a href="#" onclick="$(this).closest('form').submit()"><i  class="glyphicon glyphicon-search"></i></a>
                                        </button>
                                    </div>
                                </div>
                        <!--    </div> -->
                        </form>
                    </div>
                </div>
            </nav>
        <!-- About Section -->
        <section class="success" id="cinema">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <br><br>
                        <h2>Live Now</h2>
                        <hr class="star-primary">
                    </div>
                </div>
            </div>

        </section>

        <section id="portfolio">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 id="cinem">Em Exibição</h2>
                        <br>
                    </div>
                </div>
                <div class="row">
                    <?php
                    $espetaculos = DB::table('Espetaculos')->select('*')->orderby('id', 'asc')->get();
		    $count = 0;
                    foreach ($espetaculos as $espetaculo) {
                        $dbnome = $espetaculo -> nome;
                        $dbimage = $espetaculo -> imagem;
                        $dbdescricao = $espetaculo -> descricao;
                        $dbdatafim = $espetaculo -> datafim;
                        $dbdatainicio = $espetaculo -> datainicio;
                        $dbid = $espetaculo -> id;
                        $now = time();
                        $datei = strtotime($dbdatainicio);
                        $datef = strtotime($dbdatafim);
			 if($datei <= $now and $datef >= $now) {
			    $count = $count+1;
                            echo "<div class='col-sm-3 portfolio-item'>
                                <a href='espetaculo?id=".$dbid."' class='portfolio-link' data-toggle='modal'>
                                    <div class='caption'>
                                        <div class='caption-content'>
                                            <i class='fa fa-search-plus fa-3x'>".$dbnome."</i>
                                        </div>
                                    </div>
                                    <img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/shows/".$dbimage."' class='img-responsive ci' alt='Cabin'>
                                </a>
                           </div>";
                        }
                    }
		    if ($count == 0){
			echo "Não há espetáculos em exebição neste momento";
		    }
                    ?>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2 id="cinem">Brevemente</h2>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        $espetaculos = DB::table('Espetaculos')->select('*')->orderby('id', 'asc')->get();
			$count = 0;
                        foreach ($espetaculos as $espetaculo) {
                            $dbnome = $espetaculo -> nome;
                            $dbimage = $espetaculo -> imagem;
                            $dbdescricao = $espetaculo -> descricao;
                            $dbdatafim = $espetaculo -> datafim;
                            $dbdatainicio = $espetaculo -> datainicio;
                            $dbid = $espetaculo -> id;
                            $now = time();
                            $datei = strtotime($dbdatainicio);
                            $datef = strtotime($dbdatainicio);
                            if($datei >= $now ) {
				$count = $count + 1;
                                echo "<div class='col-sm-3 portfolio-item'>
                                <a href='espetaculo?id=".$dbid."' class='portfolio-link' data-toggle='modal'>
                                    <div class='caption'>
                                        <div class='caption-content'>
                                            <i class='fa fa-search-plus fa-3x'>".$dbnome."</i>
                                        </div>
                                    </div>
                                    <img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/shows/".$dbimage."' class='img-responsive ci' alt='Cabin'>
                                </a>
                            </div>";
                            }
                        }
			if ($count == 0){
				echo "Não há espetáculos para breve";
			}
                        ?>
                    </div>
                    </section>

                <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="modal-body">
                                        <h2>A Minha Vida de Courgette</h2>
                                        <hr class="star-primary">
                                        <img src="filme1.jpg" class="img-responsive img-centered" alt="">
                                        <h3><strong>Sinopse</strong></h3>
                                        <p>Courgette é uma alcunha curiosa para um rapaz de nove anos. Porém, a sua história única é surpreendentemente universal. Após o desaparecimento da sua mãe, Courgette torna-se amigo do polícia Raymond, que o acompanha ao orfanato, cheio de outros jovens da sua idade. No início, tem de lutar para encontrar o seu espaço nesse ambiente estranho e por vezes hostil. Mesmo assim, com a ajuda de Raymond e dos seus novos amigos, Courgette acaba por aprender a confiar e pode ser que encontre o verdadeiro amor.</p>
                                        <ul class="list-inline item-details">
                                            <a href="compra"><button type="button" class="btn btn-danger"><i class="fa fa-times"></i>Comprar Bilhetes</button></a>


                                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>Voltar</button>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="modal-body">
                                        <h2>Project Title</h2>
                                        <hr class="star-primary">
                                        <img src="img/portfolio/cake.png" class="img-responsive img-centered" alt="">
                                        <p>Use this area of the page to describe your project. The icon above is part of a free icon set by <a href="https://sellfy.com/p/8Q9P/jV3VZ/">Flat Icons</a>. On their website, you can download their free set with 16 icons, or you can purchase the entire set with 146 icons for only $12!</p>
                                        <ul class="list-inline item-details">
                                            <a href="#"><button type="button" class="btn btn-danger"><i class="fa fa-times"></i>Comprar Bilhetes</button></a>


                                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>Voltar</button>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="modal-body">
                                        <h2>Project Title</h2>
                                        <hr class="star-primary">
                                        <img src="img/portfolio/circus.png" class="img-responsive img-centered" alt="">
                                        <p>Use this area of the page to describe your project. The icon above is part of a free icon set by <a href="https://sellfy.com/p/8Q9P/jV3VZ/">Flat Icons</a>. On their website, you can download their free set with 16 icons, or you can purchase the entire set with 146 icons for only $12!</p>
                                        <ul class="list-inline item-details">
                                            <a href="#"><button type="button" class="btn btn-danger"><i class="fa fa-times"></i>Comprar Bilhetes</button></a>


                                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>Voltar</button>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="modal-body">
                                        <h2>Project Title</h2>
                                        <hr>
                                        <img src="img/portfolio/game.png" class="img-responsive img-centered" alt="">
                                        <p>Use this area of the page to describe your project. The icon above is part of a free icon set by <a href="https://sellfy.com/p/8Q9P/jV3VZ/">Flat Icons</a>. On their website, you can download their free set with 16 icons, or you can purchase the entire set with 146 icons for only $12!</p>
                                        <ul class="list-inline item-details">
                                            <a href="#"><button type="button" class="btn btn-danger"><i class="fa fa-times"></i>Comprar Bilhetes</button></a>


                                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>Voltar</button>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="modal-body">
                                        <h2>Project Title</h2>
                                        <hr class="star-primary">
                                        <img src="img/portfolio/safe.png" class="img-responsive img-centered" alt="">
                                        <p>Use this area of the page to describe your project. The icon above is part of a free icon set by <a href="https://sellfy.com/p/8Q9P/jV3VZ/">Flat Icons</a>. On their website, you can download their free set with 16 icons, or you can purchase the entire set with 146 icons for only $12!</p>
                                        <ul class="list-inline item-details">
                                            <a href="#"><button type="button" class="btn btn-danger"><i class="fa fa-times"></i>Comprar Bilhetes</button></a>


                                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>Voltar</button>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="modal-body">
                                        <h2>Project Title</h2>
                                        <hr>
                                        <img src="img/portfolio/submarine.png" class="img-responsive img-centered" alt="">
                                        <p>Use this area of the page to describe your project. The icon above is part of a free icon set by <a href="https://sellfy.com/p/8Q9P/jV3VZ/">Flat Icons</a>. On their website, you can download their free set with 16 icons, or you can purchase the entire set with 146 icons for only $12!</p>
                                        <ul class="list-inline item-details">
                                            <a href="#"><button type="button" class="btn btn-danger"><i class="fa fa-times"></i>Comprar Bilhetes</button></a>


                                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>Voltar</button>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </body>

            <!-- Footer -->
            <footer class="text-center">
                <div class="footer-above">
                    <div class="container">
                        <div class="row">
                            <div class="footer-col col-md-12">
                                <img id="logo" src="logo_branco.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-below">
                    <div class="container">
                        <div class="row">
                            <div class="footer-col col-md-12">Grupo 2 - PTI/PTR - FCUL Copyright &copy;</div>
                        </div>
                    </div>
                </div>
            </footer>

            <!-- /.container -->

            <!-- jQuery -->
            <script src="js/jquery.js"></script>

            <!-- Bootstrap Core JavaScript -->
            <script src="js/bootstrap.min.js"></script>

            </body>

        </html>

