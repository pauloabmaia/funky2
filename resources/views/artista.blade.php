<?php
$id = $_GET['id'];
$users = DB::table('UtilizadoresRegistados')->select('*')->where('id', $id)->get();
foreach ($users as $user) {
    $dbnick = $user -> nick;
    $dbnome = $user -> nome;
    $dbimagemuser = $user -> image;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" type="image/png"  href="Imagem1.png" />
<title>FunkyNotes</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Theme CSS -->
<link href="css/freelancer.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/portfolio-item.css" rel="stylesheet">
</head>
<body>
<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" role="navigation">
  <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
	  <button type="button" class="btn btn-default navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	      <span class="sr-only">FunkyNotesNav</span>
	      <span class="glyphicon glyphicon-menu-down"></span>
	  </button>
	  <a class="navbar-brand" href="index">FunkyNotes</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	<!-- dropdown -->
	<ul class="navbar-form navbar-left">
	  <div class="dropdown">
	    <button class="btn btn-default1 dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
	      <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
	    </button>
	    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
	      <li class="opcao"><a href="espetaculos">Espectáculos</a></li>
	      <li class="opcao"><a href="cinema">LiveNow</a></li>
	      <li class="opcao"><a href="musicas">Música</a></li>
	      <li class="opcao"><a href="videos">Vídeos</a></li>
	    </ul>
	  </div>
	</ul>

	<div id="terminariniciar">
	    <ul class="nav navbar-nav navbar-right nav-main" id="reglogbar">
		  <?php
		  if (isset($_COOKIE['login']) and $_COOKIE['login'] == 'true'){
		  ?>
		    <ul style="margin-top:7px; " class="navbar-nav navbar-right" id="reglogbar">
		      <li>
			<div class="dropdown">
			  <button type="button" class="btn btn-default1 dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
			  </button>
			  <ul class="dropdown-menu" role="menu">
			    <li class="opcao"><a href="divulgacao">Adicionar Espetáculos</a></li>
			    <li class="opcao"><a href="upload">Upload Musicas/Videos</a></li>
			  </ul>
			</div>
		      </li>
		    <li>
		      <button type="button" class="btn btn-link dropdown-toggle " data-toggle="dropdown">
			  <?php
		      $nick = $_COOKIE['nick'];
		      $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
		      foreach ($users as $user) {
			  $imagem = $user -> image;
		      }
		      echo"<span aria-hidden='true'><img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$imagem."' class='util img-circle' style='margin-top:-7px; margin-left:10px; '></span>";
			      ?>
			  </button>

			  <ul class="dropdown-menu" role="menu">
			      <?php
		      $nick = $_COOKIE['nick'];
		      $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
		      foreach ($users as $user) {
			  $dbid = $user -> id;
		      }
		      echo "<li><a href='artista?id=".$dbid."onClick='$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();''>Perfil</a></li>";
			      ?>
			      <li><a href="logout">Terminar Sessão</a></li>
			  </ul>

		  </li>
		  <?php
		  }
		  else{
		  ?>
		  <li><a href='registo'>Registar</a></li>";
		  <li><a href='entrar'>Entrar</a></li>
		  <?php
		  }
		  ?>
		    </ul>
	      </ul>
	  </div>

	  <form class="navbar-form navbar-left" action="search" method="get" role="form">
	      <!-- <div id="custom-search-input"> -->
		  <div class="input-group">
		      <input type="text" class="form-control" placeholder="Pesquisa" name="pesquisa"/>
		      <div class="input-group-btn">
			  <button class="btn btn-default1" type="button">
			      <a href="#" onclick="$(this).closest('form').submit()"><i  class="glyphicon glyphicon-search"></i></a>
			  </button>
		      </div>
		  </div>
	  <!--    </div> -->
	  </form>
      </div>
  </div>
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
	<div class="col-md-2">
	    <h2></h2>
	    <div class="artutil">
		<?php
		echo "<img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$dbimagemuser."' class='faceutil img-circle' alt='fotoperfil'>";
		echo "</div>";
		echo "<h4 style='text-align:center;' id='nomepessoa'>".$dbnome."</h5>";
		?>
		<div class="row">
		    <br>
		</div>
		<?php if (isset($_COOKIE['nick']) and $_COOKIE['login'] == 'true'and $dbnick == $_COOKIE['nick']){ ?>
		<div class="row">
		    <a href='upload'><button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target='upload'>Upload</button></a>
		    <br>
		    <a href='divulgacao'><button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target='upload'>Divulgação Espetáculos</button></a>
		    <br>
		    <a href='meusbilhetes'><button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target='upload'>Os Meus Bilhetes</button></a>
		</div>
		<?php }?>
	    </div>
	    <div class="col-md-1"></div>
	    <div class="col-md-9">
		<div>
		    <a href="musicas" id ="partista"><h3 class="text-center">Músicas</h3></a>
		    <hr class="star-primary">
		    <div class="scrolhor">
			<ul class="list-inline">
			    <?php
			    $musicas = DB::table('Musica')->select('*')->orderby('id', 'dsc')->where('idArtista', $id)->get();
			    foreach ($musicas as $musica) {
				$dbnome = $musica -> nome;
				$dbidmusica = $musica -> id;
				$dbdescricao = $musica -> descricao;
				$dbimagem = $musica -> image;
				echo "<li class='artistautil'>
				<div class='col-sm-3 portfolio-item'>
				    <a href='musi?id=".$dbidmusica."' class='portfolio-link' data-toggle='modal'>
					<img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/musics/images/".$dbimagem."' class='artistautil' alt='...'>
				    </a>
				</div>
			    </li>";
			    }
			    ?>
			</ul>
		    </div>
		</div>
		<div>
		    <a id ="partista" href="videos"><h3 class="text-center">Vídeos</h3></a>
		    <hr class="star-primary">
		    <div class="scrolhor">
			<ul class="list-inline">
			    <?php
			    $musicas = DB::table('Video')->select('*')->orderby('id', 'dsc')->where('idArtista', $id)->get();
			    foreach ($musicas as $musica) {
				$dbnome = $musica -> nome;
				$dbimagem = $musica -> video;
				$dbidvideo = $musica -> id;
				$dbdescricao = $musica -> descricao;
				echo "<li class='artistautil'>
				    <div class='col-sm-3 portfolio-item'>
					<a href='video?id=".$dbidvideo."' class='portfolio-link' data-toggle='modal'>
					  <video width='150' height='150' controls>
					  <source src='https://s3-eu-west-1.amazonaws.com/funkybucket1/videos/".$dbnome."' type='video/mp4'>
					  </video>
					</a>
				    </div>
				</li>";
			    }
			    ?>
			</ul>
		    </div>
		</div>
		<div>
		    <a id ="partista" href="cinema"><h3 class="text-center">Espectáculos</h3></a>
		    <hr class="star-primary">
		    <div class="scrolhor">
			<ul class="list-inline">
			    <?php
                                    $musicas = DB::table('Espetaculos')->select('*')->orderby('id', 'dsc')->where('idArtista', $id)->get();
                                    foreach ($musicas as $musica) {
                                        $dbnome = $musica -> nome;
                                        $dbimagem = $musica -> imagem;
                                        $dbidespetaculo = $musica -> id;
                                        $dbdescricao = $musica -> descricao;
                                        echo "<li class='artistautil'>
                                            <div class='col-sm-3 portfolio-item'>
                                                <a href='espetaculo?id=".$dbidespetaculo."' class='portfolio-link' data-toggle='modal'>
                                                    <img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/shows/" .$dbimagem."' class='artistautil' alt='...'>
                                                </a>
                                            </div>
                                        </li>";
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <footer class="text-center">
                <div class="footer-above">
                    <div class="container">
                        <div class="row">
                            <div class="footer-col col-md-12">
                                <img id="logo" src="logo_branco.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-below">
                    <div class="container">
                        <div class="row">
                            <div class="footer-col col-md-12">Copyright &copy; Grupo 2 - PTI/PTR - FCUL </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- /.container -->

            <!-- jQuery -->
            <script src="js/jquery.js"></script>

            <!-- Bootstrap Core JavaScript -->
            <script src="js/bootstrap.min.js"></script>

            </body>
        </html>

