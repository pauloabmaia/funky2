<?php
	$id = $_GET['id'];
	$espetaculos = DB::table('Espetaculos')->select('*')->where('id', $id)->get();
	foreach ($espetaculos as $espetaculo) {
		$dbnome = $espetaculo -> nome;
		$dbdescricao = $espetaculo -> descricao;
		$dblocal = $espetaculo -> local;
		$dbdatainicio = $espetaculo -> datainicio;
		$dbdatafim = $espetaculo -> datafim;
		$dbqtdBilhetes = $espetaculo -> qtdBilhetes;
		$dbpreco = $espetaculo -> preco;
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<link rel="icon" type="image/png"  href="../imagem1.png" />
	<title>FunkyNotes</title>

	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- Theme CSS -->
	<link href="css/freelancer.css" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="css/portfolio-item.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>

  <!-- Navigation -->
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="btn btn-default1 navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">FunkyNotesNav</span>
                        <span class="glyphicon glyphicon-menu-down"></span>
                    </button>
                    <a class="navbar-brand" href="index">FunkyNotes</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <!-- dropdown -->
                  <ul class="navbar-form navbar-left">
                    <div class="dropdown">
                      <button class="btn btn-default1 dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li class="opcao"><a href="espetaculos">Espectáculos</a></li>
                        <li class="opcao"><a href="cinema">LiveNow</a></li>
                        <li class="opcao"><a href="musicas">Música</a></li>
                        <li class="opcao"><a href="videos">Vídeos</a></li>
                      </ul>
                    </div>
                  </ul>

                  <div id="terminariniciar">
                      <ul class="nav navbar-nav navbar-right nav-main" id="reglogbar">
                            <?php
                            if (isset($_COOKIE['login']) and $_COOKIE['login'] == 'true'){
                            ?>
                              <ul style="margin-top:7px; " class="navbar-nav navbar-right" id="reglogbar">
                                <li>
                                  <div class="dropdown">
                                    <button type="button" class="btn btn-default1 dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                      <li class="opcao"><a href="divulgacao">Adicionar Espetáculos</a></li>
                                      <li class="opcao"><a href="upload">Upload Musicas/Videos</a></li>
                                    </ul>
                                  </div>
                                </li>
                              <li>
                                <button type="button" class="btn btn-link dropdown-toggle " data-toggle="dropdown">
                                    <?php
                                $nick = $_COOKIE['nick'];
                                $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                foreach ($users as $user) {
                                    $imagem = $user -> image;
                                }
                                echo"<span aria-hidden='true'><img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$imagem."' class='util img-circle' style='margin-top:-7px; margin-left:10px; '></span>";
                                        ?>
                                    </button>

                                    <ul class="dropdown-menu" role="menu">
                                        <?php
                                $nick = $_COOKIE['nick'];
                                $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                foreach ($users as $user) {
                                    $dbid = $user -> id;
					$useratualid = $user -> id;

				 }
                                echo "<li><a href='artista?id=".$dbid."onClick='$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();''>Perfil</a></li>";
                                        ?>
                                        <li><a href="logout">Terminar Sessão</a></li>
                                    </ul>

                            </li>
                            <?php
                            }
                            else{
                            ?>
                            <li><a href='registo'>Registar</a></li>";
                            <li><a href='entrar'>Entrar</a></li>
                            <?php
                            }
                            ?>
                              </ul>
                        </ul>
                    </div>

                    <form class="navbar-form navbar-left" action="search" method="get" role="form">
                        <!-- <div id="custom-search-input"> -->
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Pesquisa" name="pesquisa"/>
                                <div class="input-group-btn">
                                    <button class="btn btn-default1" type="button">
                                        <a href="#" onclick="$(this).closest('form').submit()"><i  class="glyphicon glyphicon-search"></i></a>
                                    </button>
                                </div>
                            </div>
                    <!--    </div> -->
                    </form>
                </div>
            </div>
        </nav>
	<div class="container">
		<!-- Page Header -->
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
                <div class="modal-body">
                    <h2><?php echo $dbnome; ?></h2>
                    <hr class="star-primary">
                    <?php
                        $shows = DB::table('Espetaculos')->where('id', $id)->select('*')->get();
                        foreach ($shows as $show) {
    	                	$imagem = $show -> imagem;
				$bilhetesdisponiveis = $show -> qtdBilhetes;
				$now = time();
				$datei = strtotime($dbdatainicio);
				$datef = strtotime($dbdatafim);
				$idArtista = $show -> idArtista;
                        }
			$artistas = DB::table('UtilizadoresRegistados')->where('id', $idArtista)->select('*')->get();
			foreach ($artistas as $artista){
				$nomeArtista = $artista -> nome;
			}
			$path = "https://s3-eu-west-1.amazonaws.com/funkybucket1/shows/".$imagem;
			echo "<img src='".$path."' class='img-responsive img-centered' style='width: 250px' alt='fotoperfil'>";
			?>
			<a href='artista?id=<?php echo $idArtista; ?>'><p><?php echo $nomeArtista; ?></p></a>
                    <h3><strong>Sinopse</strong></h3>
                    <p><?php echo $dbdescricao; ?></p>

                    <h3><strong>Informações</strong></h3>
                    <p>Local: <?php echo $dblocal; ?></p>
                   <p>Data e hora inicio: <?php echo $dbdatainicio; ?></p>
                    <p>Data e hora fim: <?php echo $dbdatafim; ?></p>
										<p>Bilhetes disponíveis: <?php echo $bilhetesdisponiveis; ?> </p>
										<p>Preço: <?php echo $dbpreco; ?> €</p>
                    <ul class="list-inline item-details">
				 <?php
					 if(($datei > $now and $datef > $now) and (isset($useratualid) and $useratualid != $idArtista)) {
					echo "<a href='compra?id=".$id."'<button type='button' class='btn btn-danger'><i class='fa fa-times'></i>Comprar Bilhetes</button></a>";
				}
 			
			?>
			<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="goBack()"><i class="fa fa-times"></i>Voltar</button>
                    </ul>
                </div>
            </div>

			<!--<div class="col-md-12">
				<h1 class="page-header"><?php// echo $dbnome; ?></h1>
			</div>

			<div class="col-md-2">
				<?php
					//$path = "./storage/fotoespetaculo/".$dbnome.".jpg";
					//echo "<img src='".$path."' class='img-rounded espe' style='width: 250px' alt='fotoperfil'>";
				?>
			</div>

			<div class="col-md-4 col-md-offset-1">
				<p><?php// echo $dbdescricao; ?></p>
				<p>Local: <?php //echo $dblocal; ?></p>
				<p>Data e hora: <?php //echo $dbdatainicio; ?></p>
				<p>Preço: <?php //echo $dbpreco; ?> €</p>
			</div>

			<div class="col-md-4 col-md-offset-1 text-center">
				<?php //echo "<button type='button' style='width:200px' class='btn btn-danger btn-lg'><a href='compra?id=".$id."' >Comprar Bilhete</a></button>"; ?>
				<p style="text-decoration: underline"><?php //echo $dbqtdBilhetes;?> Disponíveis</p>
			</div>-->

		</div>
	</div>
	<script>
		function goBack() {
			window.history.back();
		}
	</script>
	<!-- Footer -->
	<footer class="text-center">
		<div class="footer-above">
			<div class="container">
				<div class="row">
					<div class="footer-col col-md-12">
						<img id="logo" src="logo_branco.png">
					</div>
				</div>
			</div>
		</div>
		<div class="footer-below">
			<div class="container">
				<div class="row">
					<div class="footer-col col-md-12">Copyright &copy; Grupo 2 - PTI/PTR - FCUL </div>
				</div>
			</div>
		</div>
	</footer>
	<!-- /.container -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

</body>
</html>
