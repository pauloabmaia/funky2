<?php
$id = $_GET['id'];
$musicas = DB::table('Musica')->select('*')->where('id', $id)->get();
foreach ($musicas as $musica) {
    $dbnome = $musica -> nomemusica;
    $dbnomemusica = $musica -> nome;
    $dbdescricao = $musica -> descricao;
    $idartista = $musica -> idArtista;
    $imagemmusica = $musica -> image;
}
$users = DB::table('UtilizadoresRegistados')->select('*')->where('id', $idartista)->get();
foreach ($users as $user) {
    $dbartista = $user -> nome;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="icon" type="image/png"  href="Imagem1.png" />
        <title>FunkyNotes</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Theme CSS -->
        <link href="css/freelancer.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/portfolio-item.css" rel="stylesheet">
    </head>

    <body>
      <!-- Navigation -->
            <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="btn btn-default1 navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">FunkyNotesNav</span>
                            <span class="glyphicon glyphicon-menu-down"></span>
                        </button>
                        <a class="navbar-brand" href="index">FunkyNotes</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <!-- dropdown -->
                      <ul class="navbar-form navbar-left">
                        <div class="dropdown">
                          <button class="btn btn-default1 dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li class="opcao"><a href="espetaculos">Espectáculos</a></li>
                            <li class="opcao"><a href="cinema">LiveNow</a></li>
                            <li class="opcao"><a href="musicas">Música</a></li>
                            <li class="opcao"><a href="videos">Vídeos</a></li>
                          </ul>
                        </div>
                      </ul>

                      <div id="terminariniciar">
                          <ul class="nav navbar-nav navbar-right nav-main" id="reglogbar">
                                <?php
                                if (isset($_COOKIE['login']) and $_COOKIE['login'] == 'true'){
                                ?>
                                  <ul style="margin-top:7px; " class="navbar-nav navbar-right" id="reglogbar">
                                    <li>
                                      <div class="dropdown">
                                        <button type="button" class="btn btn-default1 dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                          <li class="opcao"><a href="divulgacao">Adicionar Espetáculos</a></li>
                                          <li class="opcao"><a href="upload">Upload Musicas/Videos</a></li>
                                        </ul>
                                      </div>
                                    </li>
                                  <li>
                                    <button type="button" class="btn btn-link dropdown-toggle " data-toggle="dropdown">
                                        <?php
                                    $nick = $_COOKIE['nick'];
                                    $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                    foreach ($users as $user) {
                                        $imagem = $user -> image;
                                    }
                                    echo"<span aria-hidden='true'><img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$imagem."' class='util img-circle' style='margin-top:-7px; margin-left:10px; '></span>";
                                            ?>
                                        </button>

                                        <ul class="dropdown-menu" role="menu">
                                            <?php
                                    $nick = $_COOKIE['nick'];
                                    $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                    foreach ($users as $user) {
                                        $dbid = $user -> id;
                                    }
                                    echo "<li><a href='artista?id=".$dbid."onClick='$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();''>Perfil</a></li>";
                                            ?>
                                            <li><a href="logout">Terminar Sessão</a></li>
                                        </ul>

                                </li>
                                <?php
                                }
                                else{
                                ?>
                                <li><a href='registo'>Registar</a></li>";
                                <li><a href='entrar'>Entrar</a></li>
                                <?php
                                }
                                ?>
                                  </ul>
                            </ul>
                        </div>

                        <form class="navbar-form navbar-left" action="search" method="get" role="form">
                            <!-- <div id="custom-search-input"> -->
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Pesquisa" name="pesquisa"/>
                                    <div class="input-group-btn">
                                        <button class="btn btn-default1" type="button">
                                            <a href="#" onclick="$(this).closest('form').submit()"><i  class="glyphicon glyphicon-search"></i></a>
                                        </button>
                                    </div>
                                </div>
                        <!--    </div> -->
                        </form>
                    </div>
                </div>
            </nav>

        <!-- About Section -->
        <section >
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h5>Artista</h5>
                    <h2><?php echo "<a href='artista?id=".$idartista."'>".$dbartista; ?></a></h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 portfolio-item">
                        <a href="" class="portfolio-link">
                            <?php
                            echo "<img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/musics/images/".$imagemmusica."' class='img-responsive'>";
                            ?>
                        </a>
                        <audio controls>
                            <?php
                            echo "<source src='https://s3-eu-west-1.amazonaws.com/funkybucket1/musics/".$dbnomemusica."' type='audio/mpeg' id='musicaplay'>";
                            ?>
                        </audio>
                        <!--<a href="download" id="download">Download </a>-->
                    </div>

                    <div class="col-sm-7 portfolio-item">
                        <h2 id="cinem"></h2>
                        <br>
                        <h3>Nome</h3>
                        <h4 id="cinem"><?php echo $dbnome; ?></h4>
                        <h3>Descrição</h3>
                        <h4><?php echo $dbdescricao; ?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 portfolio-item">
                        <h2 id="cinem">Comentários</h2>
                        <br>
                    </div>
                    <div class="container bootstrap snippet">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="blog-comment">
                                    <ul class="comments">
                                        <?php
                                        $mensagens = DB::table('Mensagens')->select('*')->where('idmusica', $id)->get();
                                        foreach ($mensagens as $mensagem) {
                                            $idmensagem = $mensagem -> id;
                                            $uid = $mensagem -> uid;
                                            $descricao_mensagem = $mensagem -> descricao;

                                            $cenas = DB::table('UtilizadoresRegistados')->select('*')->where('id', $uid)->get();
                                            foreach ($cenas as $cena) {
                                                $imagemuser = $cena -> image;
                                                $dbnomeArtista = $cena -> nome;
                                            }
                                            echo "<li class='clearfix'>
                                                <img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$imagemuser."' class='avatar' alt=''>
                                                <div class='post-comments'>
                                                    <p class='meta'><a href='artista?id=".$uid."'>".$dbnomeArtista."</a> says : <i class='pull-right'></i></p>
                                                    <p>".$descricao_mensagem."</p></div></li>";
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                    if (isset($_COOKIE['login']) and $_COOKIE['login'] == 'true'){
                                    ?>
                                    <div class="post-footer" >
                                        <div class="input-group" style="width:100%;">
                                            <form action="mensagem" method="post" role="form" enctype="multipart/form-data">
                                                <input class="form-control" name="mensagem_a_submeter" placeholder="Adicionar Comentário" type="text" style="float:left; width:90%; margin-right:1%;" required>
                                               <!--<span class="input-group-addon" >
                                                    <a href="#" onclick="$(this).closest('form').submit()"><i style="margin-left: 3%; margin-top:10%;" style="float:left" class='glyphicon glyphicon-comment'></i></a>
                                                   -->
							<button type="submit" class="btn btn-primary">
  								<i class="glyphicon glyphicon-comment"></i>
							</button>
							 <!--<i class='glyphicon glyphicon-comment'><input type="submit" name="submit"></i>-->
                                                </span>
                                                <input type="hidden" name="idmusica" value="<?php echo $id; ?>">
                                                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                                            </form>
                                        </div>
                                    </div>
                                    <?php };?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-12">
                        <img id="logo" src="logo_branco.png">
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-12">Grupo 2 - PTI/PTR - FCUL Copyright &copy;</div>
                </div>
            </div>
        </div>
    </footer>

    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</html>
