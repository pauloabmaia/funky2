<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>FunkyNotes</title>
        <link rel="icon" type="image/png"  href="../imagem1.png" />

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/freelancer.css" rel="stylesheet">
        <link href="css/portfolio-item.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
      <!-- Navigation -->
            <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="btn btn-default1 navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">FunkyNotesNav</span>
                            <span class="glyphicon glyphicon-menu-down"></span>
                        </button>
                        <a class="navbar-brand" href="index">FunkyNotes</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <!-- dropdown -->
                      <ul class="navbar-form navbar-left">
                        <div class="dropdown">
                          <button class="btn btn-default1 dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li class="opcao"><a href="espetaculos">Espectáculos</a></li>
                            <li class="opcao"><a href="cinema">LiveNow</a></li>
                            <li class="opcao"><a href="musicas">Música</a></li>
                            <li class="opcao"><a href="videos">Vídeos</a></li>
                          </ul>
                        </div>
                      </ul>

                      <div id="terminariniciar">
                          <ul class="nav navbar-nav navbar-right nav-main" id="reglogbar">
                                <?php
                                if (isset($_COOKIE['login']) and $_COOKIE['login'] == 'true'){
                                ?>
                                  <ul style="margin-top:7px; " class="navbar-nav navbar-right" id="reglogbar">
                                    <li>
                                      <div class="dropdown">
                                        <button type="button" class="btn btn-default1 dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                          <li class="opcao"><a href="divulgacao">Adicionar Espetáculos</a></li>
                                          <li class="opcao"><a href="upload">Upload Musicas/Videos</a></li>
                                        </ul>
                                      </div>
                                    </li>
                                  <li>
                                    <button type="button" class="btn btn-link dropdown-toggle " data-toggle="dropdown">
                                        <?php
                                    $nick = $_COOKIE['nick'];
                                    $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                    foreach ($users as $user) {
                                        $imagem = $user -> image;
                                    }
                                    echo"<span aria-hidden='true'><img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$imagem."' class='util img-circle' style='margin-top:-7px; margin-left:10px; '></span>";
                                            ?>
                                        </button>

                                        <ul class="dropdown-menu" role="menu">
                                            <?php
                                    $nick = $_COOKIE['nick'];
                                    $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                    foreach ($users as $user) {
                                        $dbid = $user -> id;
                                    }
                                    echo "<li><a href='artista?id=".$dbid."onClick='$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();''>Perfil</a></li>";
                                            ?>
                                            <li><a href="logout">Terminar Sessão</a></li>
                                        </ul>

                                </li>
                                <?php
                                }
                                else{
                                ?>
                                <script type="text/javascript">
                                  window.location.href = "entrar";
                                </script>
                                <!--<li><a href='registo' onClick="$('#indexbox').hide(); $('#signupbox').show(); $('#reglogbar').hide(); $('#livenow').hide();">Registar</a></li>
                                <li><a href='entrar' onClick="$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();">Entrar</a></li>-->
                                <?php
                              }
                              ?>
                                </ul>
                          </ul>
                      </div>

                      <form class="navbar-form navbar-left" action="search" method="get" role="form">
                          <!-- <div id="custom-search-input"> -->
                              <div class="input-group">
                                  <input type="text" class="form-control" placeholder="Pesquisa" name="pesquisa"/>
                                  <div class="input-group-btn">
                                      <button class="btn btn-default1" type="button">
                                          <a href="#" onclick="$(this).closest('form').submit()"><i  class="glyphicon glyphicon-search"></i></a>
                                      </button>
                                  </div>
                              </div>
                      <!--    </div> -->
                      </form>
                  </div>
              </div>
          </nav>
        <!-- Page Content -->

        <!-- Formulario de escolha add musica ou video -->
        <div id="form-1" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">Upload de músicas e vídeos</div>
                    <div style="float:right; font-size: 85%; position: relative; top:-10px"></div>
                </div>
                <div class="panel-body" >
                    <form id="addeventform" class="form-horizontal" role="form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <!-- Button -->
                            <div class="col-md-offset-2 col-md-9">
                                <button id="btn-music" type="button" class="btn btn-primary seg" onClick="$('#form-1').hide(); $('#form-2').show();"><i class="icon-hand-right"></i> Adicionar Música </button> &nbsp
                                <button id="btn-video" type="button" class="btn btn-primary seg" onClick="$('#form-1').hide(); $('#form-3').show();"><i class="icon-hand-left"></i> Adicionar Video </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <!-- formulario add musica -->
        <div id="form-2" style=" display:none; margin-bottom:5px; margin-top:10px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">Adiciona a tua musica</div>
                    <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="helpbutton" onClick="$('#form-2').hide(); $('#form-1').show();">Voltar</a></div>
                </div>
                <div class="panel-body" >
                    <form action="uploadmusica" method="post" id="addmusicform" class="form-horizontal" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <!-- Mensagem de erro -->
                        <div id="signupalert" style="display:none" class="alert alert-danger">
                            <p>Erro:</p>
                            <span></span>
                        </div>
                        <!-- /Mensagem de erro -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label for="musicname" class="col-md-3 control-label">Nome da música</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="musicname" placeholder="nome" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="musicdescrption" class="col-md-3 control-label">Descrição da música</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="musicdescrption" placeholder="descrição" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="musiccoverphoto" class="col-md-3 control-label">Foto de capa</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" id="musicphoto" name="musicphoto" value="{{ csrf_token() }}" required>
                                <p class="help-block">Inserir foto individual, grupo de fotos não é aceite.</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="music" class="col-md-3 control-label">Música</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" id="music" name="music" label="music" value="{{ csrf_token() }}" required>
                                <p class="help-block">Inserir música individual, grupo de músicas não é aceite.</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <!-- Button -->
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-signup" type="submit" class="btn btn-info seg"><i class="icon-hand-right"></i> Adicionar Música </button>
                            </div>
                            <!-- /Button -->
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- formulario add Video -->
        <div id="form-3" style=" display:none; margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">Adiciona o teu video</div>
                    <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="helpbutton" onClick="$('#form-3').hide(); $('#form-1').show();">Voltar</a></div>
                </div>
                <div class="panel-body" >
                    <form action="uploadvideo" method="post" id="addvideoform" class="form-horizontal" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <!-- Mensagem de erro -->
                        <div id="signupalert" style="display:none" class="alert alert-danger">
                            <p>Erro:</p>
                            <span></span>
                        </div>
                        <!-- /Mensagem de erro -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="videoname" class="col-md-3 control-label">Nome do video</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="videoname" placeholder="nome" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="videodescrption" class="col-md-3 control-label">Descrição do video</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="videodescrption" placeholder="descrição" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="video" class="col-md-3 control-label">Video</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" id="video" name="video" value="{{ csrf_token() }}" required>
                                <p class="help-block">Inserir video individual, grupo de videos não é aceite.</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <!-- Button -->
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-signup" type="submit" class="btn btn-info seg"><i class="icon-hand-right"></i> &nbsp Adicionar Video &nbsp</button>
                            </div>
                            <!-- /Button -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Footer -->
        <footer class="text-center" style="margin-top:35%;">
            <div class="footer-above">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-md-12">
                            <img id="logo" src="logo_branco.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-below">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-md-12">Copyright &copy; Grupo 2 - PTI/PTR - FCUL </div>
                    </div>
                </div>
            </div>
        </footer>

        <!-- jQuery -->
        <script src="../js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

    </body>

</html>


