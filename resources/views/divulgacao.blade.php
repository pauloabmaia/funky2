<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/png"  href="Imagem1.png" />
    <title>FunkyNotes</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <!-- Theme CSS -->
    <link href="css/freelancer.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/portfolio-item.css" rel="stylesheet">
  </head>
	<body>
    <!-- Navigation -->
          <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" role="navigation">
              <div class="container">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                      <button type="button" class="btn btn-default1 navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                          <span class="sr-only">FunkyNotesNav</span>
                          <span class="glyphicon glyphicon-menu-down"></span>
                      </button>
                      <a class="navbar-brand" href="index">FunkyNotes</a>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <!-- dropdown -->
                    <ul class="navbar-form navbar-left">
                      <div class="dropdown">
                        <button class="btn btn-default1 dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                          <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                          <li class="opcao"><a href="espetaculos">Espectáculos</a></li>
                          <li class="opcao"><a href="cinema">LiveNow</a></li>
                          <li class="opcao"><a href="musicas">Música</a></li>
                          <li class="opcao"><a href="videos">Vídeos</a></li>
                        </ul>
                      </div>
                    </ul>

                    <div id="terminariniciar">
                        <ul class="nav navbar-nav navbar-right nav-main" id="reglogbar">
                              <?php
                              if (isset($_COOKIE['login']) and $_COOKIE['login'] == 'true'){
                              ?>
                                <ul style="margin-top:7px; " class="navbar-nav navbar-right" id="reglogbar">
                                  <li>
                                    <div class="dropdown">
                                      <button type="button" class="btn btn-default1 dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                        <li class="opcao"><a href="divulgacao">Adicionar Espetáculos</a></li>
                                        <li class="opcao"><a href="upload">Upload Musicas/Videos</a></li>
                                      </ul>
                                    </div>
                                  </li>
                                <li>
                                  <button type="button" class="btn btn-link dropdown-toggle " data-toggle="dropdown">
                                      <?php
                                  $nick = $_COOKIE['nick'];
                                  $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                  foreach ($users as $user) {
                                      $imagem = $user -> image;
                                  }
                                  echo"<span aria-hidden='true'><img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$imagem."' class='util img-circle' style='margin-top:-7px; margin-left:10px; '></span>";
                                          ?>
                                      </button>

                                      <ul class="dropdown-menu" role="menu">
                                          <?php
                                  $nick = $_COOKIE['nick'];
                                  $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                  foreach ($users as $user) {
                                      $dbid = $user -> id;
                                  }
                                  echo "<li><a href='artista?id=".$dbid."onClick='$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();''>Perfil</a></li>";
                                          ?>
                                          <li><a href="logout">Terminar Sessão</a></li>
                                      </ul>

                              </li>
                              <?php
                              }
                              else{
                              ?>
                              <li><a href='registo'>Registar</a></li>";
                              <li><a href='entrar'>Entrar</a></li>
                              <?php
                              }
                              ?>
                                </ul>
                          </ul>
                      </div>

                      <form class="navbar-form navbar-left" action="search" method="get" role="form">
                          <!-- <div id="custom-search-input"> -->
                              <div class="input-group">
                                  <input type="text" class="form-control" placeholder="Pesquisa" name="pesquisa"/>
                                  <div class="input-group-btn">
                                      <button class="btn btn-default1" type="button">
                                          <a href="#" onclick="$(this).closest('form').submit()"><i  class="glyphicon glyphicon-search"></i></a>
                                      </button>
                                  </div>
                              </div>
                      <!--    </div> -->
                      </form>
                  </div>
              </div>
          </nav>
		<!-- Page Content -->
    <div class="container">
		<div id="addevent" style="margin-top:4%;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">Adicionar Evento</div>
				</div>
				<div class="panel-body" >
					<form method='post' action='divulgar' id='addeventform' class='form-horizontal' role='form' enctype="multipart/form-data">
						{{ csrf_field() }}
						<div id="signupalert" style="display:none" class="alert alert-danger">
							<p>Erro:</p>
							<span></span>
						</div>

						<div class="form-group">
							<label for="eventname" class="col-md-3 control-label">Nome Do Evento</label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="eventname" placeholder="nome" required>
							</div>
						</div>
						<div class="form-group">
							<label for="eventdescrption" class="col-md-3 control-label">Descrição do evento</label>
							<div class="col-md-9">
								<!--<input type="textarea" class="form-control" name="eventdescrption" placeholder="descrição">-->
								<textarea class="form-control" name="eventdescrption" placeholder="descrição" required></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="initeventDate" class="col-sm-3 control-label">Data e hora de inicio do Evento </label>
							<div class="col-sm-9">
								<input type="datetime-local" id="birthDate" name="initeventdate" class="form-control" placeholder="AAAA-MM-DD HH:mm:ss">
							</div>
						</div>
						<div class="form-group">
							<label for="endeventDate" class="col-sm-3 control-label">Data e hora de fim do Evento </label>
							<div class="col-sm-9">
								<input type="datetime-local" id="birthDate" name="endeventdate" class="form-control" placeholder="AAAA-MM-DD HH:mm:ss" required>
							</div>
						</div>
						<div class="form-group">
							<label for="local" class="col-md-3 control-label">Local</label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="local" placeholder="nome do local" required>
							</div>
						</div>
					            <div>
        					    <div class="form-group">
							<label for="tickets" class="col-md-3 control-label">Tamanho da Sala</label>
              <select id='sala' name='sala' required>
                <option disabled selected value>Tamanho da Sala</option>
                <option value="5x5">5 x 5</option>
                <option value="7x7">7 x 7</option>
                <option value="15x15">15 x 15</option>
                <option value="0x0">Plateia em pé</option>
                <option value="personalizada">Personalizada</option>
              </select>

              <div id="personalizada">
                <div class="col-md-3" style="float:left;">
  								<input id="linhas" type="number" class="form-control" name="linhas" placeholder="Linhas">
  							</div>
  							<div class="col-md-3" style="float:left;">
  								<input id="colunas" type="number" class="form-control" name="colunas" placeholder="Colunas">
  							</div>
              </div>
              <div id="plateiape">
    							<div class="col-md-9">
    								<input type="number" min="0" class="form-control" name="tickets" placeholder="Bilhetes disponíveis">
    						</div>
              </div>
						</div>
</div>
						<div class="form-group">
							<label for="eventphoto" class="col-md-3 control-label">Foto do Evento</label>
							<div class="col-md-9">
								<input type="file" class="form-control" id="eventphoto" name="eventphoto">
								<p class="help-block">Inserir foto individual, grupo de fotos não é aceite.</p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3" for="ticketprice">Preço por bilhete</label>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="radio-inline">
											<input name="preco" id="free" value="free" checked="checked" onclick="$('#precoporbilhete').hide()" type="radio">Grátis
										</label>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<label class="radio-inline">
											<input name="preco" id="precodef" value="precodef" type="radio" onclick="$('#precoporbilhete').show()">Preço definido
										</label>
									</div>
									<div class="col-sm-6" id="precoporbilhete" style="display:none">
										<input type="number" class="form-control" id="priceticket" placeholder="preço" name="precobilhete" step="0.01">
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<!-- Button -->
							<div class="col-md-offset-3 col-md-6">
								<button id="btn-signup" type="submit" class="btn btn-primary seg">Adicionar Evento</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
  </div>
    <!-- Footer -->
    <footer class="text-center">
  		<div class="footer-above">
  			<div class="container">
  				<div class="row">
  					<div class="footer-col col-md-12">
  						<img id="logo" src="logo_branco.png">
  					</div>
  				</div>
  			</div>
  		</div>
  		<div class="footer-below">
  			<div class="container">
  				<div class="row">
  					<div class="footer-col col-md-12">Copyright &copy; Grupo 2 - PTI/PTR - FCUL </div>
  				</div>
  			</div>
  		</div>
  	</footer>
    <!-- /.container -->
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>

