<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png"  href="Imagem1.png" />

        <title>FunkyNotes</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Theme CSS -->
        <link href="css/freelancer.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/portfolio-item.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
      <!-- Navigation -->
            <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="btn btn-default1 navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">FunkyNotesNav</span>
                            <span class="glyphicon glyphicon-menu-down"></span>
                        </button>
                        <a class="navbar-brand" href="index">FunkyNotes</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <!-- dropdown -->
                      <ul class="navbar-form navbar-left">
                        <div class="dropdown">
                          <button class="btn btn-default1 dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li class="opcao"><a href="espetaculos">Espectáculos</a></li>
                            <li class="opcao"><a href="cinema">LiveNow</a></li>
                            <li class="opcao"><a href="musicas">Música</a></li>
                            <li class="opcao"><a href="videos">Vídeos</a></li>
                          </ul>
                        </div>
                      </ul>

                      <div id="terminariniciar">
                          <ul class="nav navbar-nav navbar-right nav-main" id="reglogbar">
                                <?php
                                if (isset($_COOKIE['login']) and $_COOKIE['login'] == 'true'){
                                ?>
                                  <ul style="margin-top:7px; " class="navbar-nav navbar-right" id="reglogbar">
                                    <li>
                                      <div class="dropdown">
                                        <button type="button" class="btn btn-default1 dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                          <li class="opcao"><a href="divulgacao">Adicionar Espetáculos</a></li>
                                          <li class="opcao"><a href="upload">Upload Musicas/Videos</a></li>
                                        </ul>
                                      </div>
                                    </li>
                                  <li>
                                    <button type="button" class="btn btn-link dropdown-toggle " data-toggle="dropdown">
                                        <?php
                                    $nick = $_COOKIE['nick'];
                                    $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                    foreach ($users as $user) {
                                        $imagem = $user -> image;
                                    }
                                    echo"<span aria-hidden='true'><img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$imagem."' class='util img-circle' style='margin-top:-7px; margin-left:10px; '></span>";
                                            ?>
                                        </button>

                                        <ul class="dropdown-menu" role="menu">
                                            <?php
                                    $nick = $_COOKIE['nick'];
                                    $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                    foreach ($users as $user) {
                                        $dbid = $user -> id;
                                    }
                                    echo "<li><a href='artista?id=".$dbid."onClick='$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();''>Perfil</a></li>";
                                            ?>
                                            <li><a href="logout">Terminar Sessão</a></li>
                                        </ul>

                                </li>
                                <?php
                                }
                                else{
                                ?>
                                <li><a href='registo'>Registar</a></li>
                                <?php
                                }
                                ?>
                                  </ul>
                            </ul>
                        </div>

                        <form class="navbar-form navbar-left" action="search" method="get" role="form">
                            <!-- <div id="custom-search-input"> -->
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Pesquisa" name="pesquisa"/>
                                    <div class="input-group-btn">
                                        <button class="btn btn-default1" type="button">
                                            <a href="#" onclick="$(this).closest('form').submit()"><i  class="glyphicon glyphicon-search"></i></a>
                                        </button>
                                    </div>
                                </div>
                        <!--    </div> -->
                        </form>
                    </div>
                </div>
            </nav>
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info" >
                <div class="panel-heading">
                    <div class="panel-title">Entrar</div>
                </div>

                <div style="padding-top:30px" class="panel-body" >
                    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                    <form action="login" method="post" id="loginform" class="form-horizontal" role="form">
                        {{ csrf_field() }}
                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input id="login-username" id = "username" type="text" class="form-control" name="username" value="" placeholder="username ou email" required>
                        </div>

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input id="login-password" type="password" class="form-control" name="password" placeholder="password" required>

                        </div>
                        <div style="float:right; font-size: 90%; position: relative; top:-10px"><a href="#">Esqueceu a password?</a></div>

                        <div class="form-group">
                            <!-- Button -->
                            <div class="col-md-12">
                                <label>
                                    <input style="float:left" id="login-remember" type="checkbox" name="remember" value="1">&nbsp;&nbsp; Lembrar-me
                                </label>
                                <button id="btn-signup" type="submit" class="btn btn-primary seg reglog">Entrar</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 control">
                                <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%"> Ainda nao tem uma conta?
                                    <a href="registo" onClick="$('#loginbox').hide(); $('#signupbox').show()"> Registe-se aqui </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <br><br><br>
                        <p>Grupo 2 - PTI/PTR</p>
                    </div>
                </div>
                <!-- /.row -->
            </footer>
        </div>
        <!-- jQuery -->
        <script src="../js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>
    </body>
</html>

