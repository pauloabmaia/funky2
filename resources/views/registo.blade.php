<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>FunkyNotes</title>
        <link rel="icon" type="image/png"  href="Imagem1.png" />

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Theme CSS -->
        <link href="css/freelancer.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/portfolio-item.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    </head>
    <body>
      <!-- Navigation -->
            <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="btn btn-default1 navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">FunkyNotesNav</span>
                            <span class="glyphicon glyphicon-menu-down"></span>
                        </button>
                        <a class="navbar-brand" href="index">FunkyNotes</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <!-- dropdown -->
                      <ul class="navbar-form navbar-left">
                        <div class="dropdown">
                          <button class="btn btn-default1 dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li class="opcao"><a href="espetaculos">Espectáculos</a></li>
                            <li class="opcao"><a href="cinema">LiveNow</a></li>
                            <li class="opcao"><a href="musicas">Música</a></li>
                            <li class="opcao"><a href="videos">Vídeos</a></li>
                          </ul>
                        </div>
                      </ul>

                      <div id="terminariniciar">
                          <ul class="nav navbar-nav navbar-right nav-main" id="reglogbar">
                                <?php
                                if (isset($_COOKIE['login']) and $_COOKIE['login'] == 'true'){
                                ?>
                                  <ul style="margin-top:7px; " class="navbar-nav navbar-right" id="reglogbar">
                                    <li>
                                      <div class="dropdown">
                                        <button type="button" class="btn btn-default1 dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                          <li class="opcao"><a href="divulgacao">Adicionar Espetáculos</a></li>
                                          <li class="opcao"><a href="upload">Upload Musicas/Videos</a></li>
                                        </ul>
                                      </div>
                                    </li>
                                  <li>
                                    <button type="button" class="btn btn-link dropdown-toggle " data-toggle="dropdown">
                                        <?php
                                    $nick = $_COOKIE['nick'];
                                    $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                    foreach ($users as $user) {
                                        $imagem = $user -> image;
                                    }
                                    echo"<span aria-hidden='true'><img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$imagem."' class='util img-circle' style='margin-top:-7px; margin-left:10px; '></span>";
                                            ?>
                                        </button>

                                        <ul class="dropdown-menu" role="menu">
                                            <?php
                                    $nick = $_COOKIE['nick'];
                                    $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                    foreach ($users as $user) {
                                        $dbid = $user -> id;
                                    }
                                    echo "<li><a href='artista?id=".$dbid."onClick='$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();''>Perfil</a></li>";
                                            ?>
                                            <li><a href="logout">Terminar Sessão</a></li>
                                        </ul>

                                </li>
                                <?php
                                }
                                else{
                                  ?>
                            <li><a href='entrar' onClick="$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();">Entrar</a></li>
                            <?php
                            }
                            ?>
                              </ul>
                        </ul>
                    </div>

                    <form class="navbar-form navbar-left" action="search" method="get" role="form">
                        <!-- <div id="custom-search-input"> -->
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Pesquisa" name="pesquisa"/>
                                <div class="input-group-btn">
                                    <button class="btn btn-default1" type="button">
                                        <a href="#" onclick="$(this).closest('form').submit()"><i  class="glyphicon glyphicon-search"></i></a>
                                    </button>
                                </div>
                            </div>
                    <!--    </div> -->
                    </form>
                </div>
            </div>
        </nav>


        <!-- Registar/login -->
        <div class="container">
            <div id="signupbox" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">Registar</div>
                    </div>
                    <div class="panel-body" >
                        <form action="regist" method="post" id="signupform" class="form-horizontal" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div id="signupalert" style="display:none" class="alert alert-danger">
                                <p>Erro:</p>
                                <span></span>
                            </div>

                            <div class="form-group">
                                <label for="firstname" class="col-md-3 control-label">Nome *</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="firstname" placeholder="nome" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="username" class="col-md-3 control-label">Nome de Utilizador *</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="username" placeholder="nome de utilizador" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="birthDate" class="col-sm-3 control-label">Data de Nascimento *</label>
                                <div class="col-sm-9">
                                    <input type="date" id="birthDate" name="dateofbirth" class="form-control" placeholder="AAAA-MM-DD" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="country" class="col-sm-3 control-label">País *</label>
                                <div class="col-sm-9">
                                    <select id="country" class="form-control" name ="country" required>
                                        <option>Portugal</option>
                                        <option>Brasil</option>
                                        <option>Espanha</option>
                                        <option>França</option>
                                        <option>Itália</option>
                                        <option>Outro</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="gender">Género *</label>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="radio-inline">
                                                <input name="gender" id="maleRadio" value="Male" checked="checked" type="radio">Masculino
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="radio-inline">
                                                <input name="gender" id="femaleRadio" value="Female" type="radio">Feminino
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="photo" class="col-md-3 control-label">Foto *</label>
                                <div class="col-md-9">
                                    <input type="file" class="form-control" id="photo" name="avatar" required>
                                    <p class="help-block">Inserir foto individual, grupo de fotos não é aceite.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-md-3 control-label">Email *</label>
                                <div class="col-md-9">
                                    <input type="email" class="form-control" name="email" placeholder="endereço de email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-md-3 control-label">Palavra-passe *</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control" name="passwd" placeholder="palavra-passe" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-md-3 control-label">Repita a Palavra-passe *</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control" name="repeatpasswd" placeholder="palavra-passe" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="checkbox">
                                        <label>
					    <p><font size="2"> * - campos de preenchimento obrigatório</font></p>
                                            <input type="checkbox" required>Aceito os <a data-toggle="modal" data-target="#termosecond">termos e condições</a> de uso
                                        </label>
                                        <div class="modal fade" id="termosecond" tabindex="-1" role="dialog"  aria-hidden="true">

                                            <div class="modal-dialog modal-md" role="document">

                                                <div id="termosecond" class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title"><span><i class="glyphicon"></i></span> Termos e Condições</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="container">
                                                            <div class="row">
                                                                <!-- You can make it whatever width you want. I'm making it full width
on <= small devices and 4/12 page width on >= medium devices -->
                                                                <div class="col-xs-5  col-md-5">
                                                                    <div style="width: 500px; height: 400px; overflow-y: scroll; overflow-x: scroll;">
                                                                        Termos e Condições do Site Online FunkyNotes <br><br>
                                                                        1.	Âmbito e Objeto das Condições Gerais da Loja
                                                                        As presentes Condições Gerais destinam-se, com o formulário de encomenda, e os demais elementos referidos nas mesmas, a regular os termos e as condições por que se regerá a prestação do Serviço Loja Online FunkyNotes (“Serviço” ou “Loja”) pela FunkyNotes, com sede na Faculdade de Ciências - DI, doravante designadas "FunkyNotes ".
                                                                        O Serviço consiste na disponibilização, através do endereço INDICAR de acesso à Loja Online que, além de fornecer informação relativa a um conjunto de produtos e/ou serviços, permite ao Utilizador, por via eletrónica, encomendar os produtos nela divulgados, nos termos e condições aqui descritos.
                                                                        A encomenda de produtos deve ser feita por Utilizadores com idade igual ou superior a 18 (dezoito) anos (indivíduos com idade inferior terão de ter autorização dos seus representantes). Os elementos e informações transmitidos pelo Utilizador gozarão de plenos efeitos jurídicos, reconhecendo o Utilizador as aquisições eletrónicas, não podendo este alegar a falta de assinatura para incumprimento das obrigações assumidas.
                                                                        <br><br>2.	Informação de Produto e Conteúdos
                                                                        A FunkyNotes fará todos os possíveis para que a informação apresentada não contenha erros tipográficos, sendo que serão rapidamente corrigidos sempre que estes ocorram. Caso adquira um produto que tenha características diferentes às apresentadas online, tem o direito de proceder à resolução do contrato de compra nos termos legais aplicáveis (direito de livre resolução - ver ponto 9).
                                                                        A FunkyNotes fará todos os possíveis para enviar a totalidade dos produtos encomendados, mas é possível que, em determinados casos e devido a causas dificilmente controláveis pela FunkyNotes, como erros humanos ou incidências nos sistemas informáticos, não seja possível disponibilizar algum dos produtos pedidos pelo Utilizador. Caso algum produto não esteja disponível depois de ter realizado a encomenda, será avisado, por correio eletrónico ou através de telefone. Nesse momento ser-lhe-á apresentada a possibilidade de anular a encomenda com o respetivo reembolso, caso já tenha efetuado o respetivo pagamento.
                                                                        Todas as informações sobre preço, produtos, especificações, ações promocionais e serviços poderão ser alterados a qualquer momento pela FunkyNotes.
                                                                        <br><br>3.	Responsabilidades
                                                                        <br><br>3.1 Todos os produtos e serviços comercializados na Loja Online FunkyNotes estão de acordo com a Lei Portuguesa.
                                                                        <br><br>3.2 A Loja possui os adequados níveis de segurança, contudo a FunkyNotes não será responsável por quaisquer prejuízos sofridos pelo Utilizador e/ou por terceiros, em virtude de atrasos, interrupções, erros e suspensões de comunicações que tenham origem em fatores fora do seu controlo, nomeadamente, quaisquer deficiências ou falhas provocadas pela rede de comunicações ou serviços de comunicações prestados por terceiros, pelo sistema informático, pelos modems, pelo software de ligação ou eventuais vírus informáticos ou decorrentes do descarregamento (“download”) através do serviço de ficheiros infetados ou contendo vírus ou outras propriedades que possam afetar o equipamento do Utilizador. Se por algum motivo de erro de acesso ao sítio eletrónico da Loja Online FunkyNotes houver impossibilidade de prestação de serviço, a FunkyNotes não será responsável por eventuais prejuízos.
                                                                        <br><br>3.3 As consultas de dados e informação efetuadas no âmbito deste Serviço, presumem-se efetuadas pelo Utilizador, declinando a FunkyNotes qualquer responsabilidade decorrente a utilização abusiva ou fraudulenta das informações obtidas.
                                                                        <br><br>3.4 A FunkyNotes não será responsável por quaisquer perdas ou danos causados por utilizações abusivas do Serviço que lhe não sejam diretamente imputáveis a título de dolo ou culpa grave.
                                                                        <br><br>3.5 A FunkyNotes não é responsável pelos prejuízos ou danos decorrentes do incumprimento ou cumprimento defeituoso do Serviço quando tal não lhe seja direta ou indiretamente imputável a título de dolo ou culpa grave, não se responsabilizando designadamente por (i) erros, omissões ou outras imprecisões relativos às informações disponibilizadas através do Serviço; (ii) danos causados por culpa do Utilizador ou de terceiros, incluindo as violações da propriedade intelectual, (iii) pelo incumprimento ou cumprimento defeituoso que resulte do cumprimento de decisões judiciais ou de autoridades administrativas ou (iv) pelo incumprimento ou cumprimento defeituoso que resulte da ocorrência de situações de força maior, ou seja, situações de natureza extraordinária ou imprevisível, exteriores à FunkyNotes e que pela mesma não possam ser controladas, tais como incêndios, cortes de energia, explosões, guerras, tumultos, insurreições civis, decisões governamentais, greves, terramotos, inundações ou outros cataclismos naturais ou outras situações não controláveis pela FunkyNotes que impeçam ou prejudiquem o cumprimento das obrigações assumidas.
                                                                        <br><br>3.6. A FunkyNotes não garante que:
                                                                        <br><br>1.	i) o Serviço seja fornecido de forma ininterrupta, seja seguro, sem erros ou funcione de forma infinita;
                                                                        <br><br>2.	ii) a qualidade de qualquer produto, serviço, informação ou qualquer outro material comprado ou obtido através do Serviço preencha quaisquer expectativa do Utilizador em relação ao mesmo;
                                                                        •	iii) qualquer material obtido de qualquer forma através da utilização do Serviço é utilizado por conta e risco do Utilizador, sendo este o único responsável por qualquer dano causado ao seu sistema e equipamento informático ou por qualquer perda de dados que resultem dessa operação.
                                                                        <br><br>1.	iv) nenhum conselho ou informação, quer oral quer escrita, obtida pelo Utilizador de ou através do Serviço criará qualquer garantia que não esteja expressa nestas Condições Gerais.
                                                                        <br><br>3.7. O Utilizador aceita que a FunkyNotes não pode de forma nenhuma ser responsabilizada por qualquer dano, incluindo, mas não limitado a, danos por perdas de lucros, dados, conteúdos, ou quaisquer outras perdas (mesmo que tenha sido previamente avisado pelo Utilizador sobre a possibilidade da ocorrência desses danos), resultantes:
                                                                        <br><br>1.	i) do uso ou impossibilidade de uso do Serviço;
                                                                        <br><br>2.	ii) da dificuldade de obtenção de qualquer substituto de bens/serviços;
                                                                        <br><br>•	iii) do acesso ou modificação não autorizado a bases de dados pessoais.
                                                                        <br><br>4.	Obrigações do Consumidor
                                                                        <br><br>4.1. O utilizador compromete-se a:
                                                                        <br><br>1.	i) Facultar dados pessoais e moradas corretas;
                                                                        <br><br>2.	ii) Não utilizar identidades falsas;
                                                                        •	iii) Respeitar os limites de encomendas impostos.
                                                                        <br><br>4.2. Caso algum dos dados esteja incorreto ou seja insuficiente, e por esse motivo haja um atraso ou impossibilidade no processamento da encomenda, ou eventual não entrega, a responsabilidade é do Utilizador, sendo que a FunkyNotes declina qualquer responsabilidade. No caso de o consumidor violar alguma destas obrigações, a FunkyNotes reserva-se no direto de eliminar futuras compras, bloquear o acesso à loja, cancelar o fornecimento de quaisquer outros serviços disponibilizados em simultâneo pela FunkyNotes ao mesmo Utilizador; e, ainda, não permitir o acesso futuro do Utilizador a algum ou quaisquer serviços disponibilizados pela FunkyNotes.
                                                                        <br><br>4.3. É expressamente vedada a utilização dos produtos e serviços adquiridos para fins comerciais, designadamente para efeitos de revenda de bens.
                                                                        <br><br>5.	Privacidade e Proteção de Dados Pessoais
                                                                        <br><br>5.1. A FunkyNotes garante a confidencialidade de todos os dados fornecidos pelos Utilizadores.
                                                                        <br><br>5.2. Os dados pessoais identificados no formulário de encomenda como sendo de fornecimento obrigatório são indispensáveis à prestação do Serviço pela FunkyNotes. A omissão ou inexatidão dos dados fornecidos pelo Utilizador são da sua única e inteira responsabilidade e podem dar lugar à recusa de prestação do Serviço pela FunkyNotes.
                                                                        <br><br>5.3. Os dados pessoais do Utilizador serão processados e armazenados informaticamente e destinam-se a ser utilizados pela FunkyNotes no âmbito da relação contratual e/ou comercial com o Utilizador e, em caso de autorização pelo Utilizador, para a comercialização de INDICAR CASO A CASO E DE ACORDO COM O OBJETIVO DA EMPRESA EM CONCRETO
                                                                        <br><br>5.4. Nos termos da legislação aplicável, é garantido ao Utilizador, sem encargos adicionais, o direito de acesso, retificação e atualização dos seus dados pessoais, diretamente ou mediante pedido por escrito, bem como o direito de oposição à utilização dos mesmos para as finalidades previstas no número anterior, devendo para o efeito contactar a entidade responsável pelo tratamento dos dados pessoais: FunkyNotes.
                                                                        <br><br>5.5. A Internet é uma rede aberta, pelo que os  dados pessoais do Utilizador, demais informações pessoais e todo o conteúdo alojado no Serviço poderão circular na rede sem condições de segurança, correndo, inclusive, o risco de serem acessíveis e utilizados por terceiros não autorizados para o efeito, não podendo a FunkyNotes ser responsabilizada por esse acesso e/ou utilização.
                                                                        <br><br>6.	Cancelamento de encomendas
                                                                        <br><br>6.1 A pedido do Utilizador
                                                                        O Utilizador poderá efetuar o cancelamento da sua encomenda solicitando-o à FunkyNotes através do número de telefone ou e-mail referindo o número da encomenda, o qual será aceite desde que a mesma ainda não tenha sido processada. Após o seu processamento, a FunkyNotes tentará efetuar a entrega da mesma, mas o Utilizador tem a opção de não a aceitar.
                                                                        Para o efeito de cancelamento o Utilizador deverá indicar os seguintes dados à FunkyNotes:
                                                                        <br><br>1.	a) Número da encomenda
                                                                        <br><br>2.	b) NIF com que efetuou a encomenda e morada de entrega
                                                                        <br><br>6.2 Por decisão da FunkyNotes
                                                                        A FunkyNotes reserva-se no direito de não processar encomendas, quando verificar alguma inconsistência nos dados pessoais apresentados ou observar má conduta por parte do comprador. A FunkyNotes reserva-se no direito de não efetuar o processamento de qualquer encomenda ou reembolso, no caso de se verificarem erros nos valores e/ou características dos produtos, quando estes decorrerem de problemas técnicos ou erros alheios à FunkyNotes.
                                                                        <br><br>7.	Devolução (Direito de Resolução)
                                                                        <br><br>7.1. O Utilizador, no caso de ser consumidor, pode exercer o direito de resolução sem que lhe seja exigida qualquer indemnização, no prazo de 14 (catorze) dias a contar do dia em que o consumidor adquira a posse física do bem.
                                                                        Para exercer este direito, o Utilizador poderá usar a minuta indicada abaixo, devendo indicar todos os seus dados de identificação, o serviço subscrito que pretende resolver e a data de subscrição. A comunicação deverá ser feita, por carta, através da devolução do bem adquirido, ou por outro meio adequado e suscetível de prova dentro do prazo acima definido.
                                                                        O consumidor deve no prazo de 14 (catorze) dias a contar da data da comunicação da resolução devolver os bens à FunkyNotes nas devidas condições de utilização.
                                                                        <br><br>7.2. Após receção da devolução na FunkyNotes será devolvido ao Utilizador o valor correspondente ao valor pago pela encomenda (valor da fatura de venda). Caso tenha utilizado um código de desconto promocional, esse valor não será restituído, ou seja, o reembolso será apenas pelo valor efetivamente pago.
                                                                        <br><br>7.3. O método de reembolso do valor a devolver depende do método de pagamento utilizado na respetiva encomenda. No caso de pagamentos com cartão de crédito e PayPal, estes são creditados nas respetivas contas. Nos restantes casos, quando é fornecida informação do NIB, o reembolso é feito para a conta bancária indicada. Caso contrário, o reembolso é realizado por cheque para a morada de faturação. O reembolso é efetuado até 14 dias após a receção da vontade de livre resolução e da receção da devolução do bem.
                                                                        <br><br>7.4. Na falta de qualquer dos componentes do artigo vendido ou, caso qualquer deles não se encontre em excelente estado de conservação, não haverá lugar a qualquer reembolso do preço ou dos portes, sendo o produto reenviado novamente para a morada de expedição inicial.
                                                                        <br><br>8.	Defeito de fabrico
                                                                        <br><br>8.1. Em caso de "defeito de fabrico", ou seja, quando são detetadas avarias nos equipamentos que, em princípio, não se enquadrem no âmbito da respetiva garantia, o Utilizador deverá devolver o equipamento, juntamente com uma cópia da fatura e o formulário “Pedido de Troca /Devolução do Equipamento” preenchido, no prazo máximo de 30 dias consecutivos a contar da data da fatura, para a seguinte morada:
                                                                        FunkyNotes
                                                                        Faculdade de Ciências - DI
                                                                        Se o Utilizador optar por outras formas de devolução, os respetivos custos com portes de envio serão da sua responsabilidade.
                                                                        <br><br>8.2. Para que a troca do produto possa ser efetuada, deverá assegurar que a embalagem se encontra completa (caixa, manual de instruções, certificado de garantia, terminal e acessórios) contendo todos os componentes que o constituem, em excelente estado de conservação.
                                                                        <br><br>8.3. Na falta de qualquer um dos elementos referidos anteriormente, ou caso algum dos componentes não se encontre em excelente estado de conservação, não haverá lugar a qualquer troca, sendo o produto reenviado novamente ao Utilizador.
                                                                        <br><br>9.Garantia
                                                                        <br><br>9.1. Todos os equipamentos disponíveis na Loja estão devidamente certificados pelas entidades internacionais competentes.
                                                                        <br><br>9.2. Os equipamentos e acessórios possuem um período de garantia definido pelo fabricante, que nos termos legais é, no mínimo, de 2 (dois) anos. Este período é considerado a partir da data da fatura do equipamento e só pode ser exercido mediante apresentação do certificado de garantia e/ou comprovativo de compra (fatura) devidamente preenchidos.
                                                                        <br><br>9.3. São considerados fora das condições de garantia os equipamentos que tenham ultrapassado o período definido pelo fabricante ou apresentem defeitos causados por desgaste anormal, instalação imprópria, intempéries, descargas elétricas, negligência ou acidentes, mau manuseamento, infiltração de humidade/líquidos, utilização de acessórios não originais e intervenções técnicas por pessoal não autorizado.
                                                                        <br><br>9.4. Se o equipamento se avariar, e se estiver abrangido pela garantia, o Utilizador poderá dirigir-se com o mesmo, e respetivo comprovativo de compra e/ou garantia, a um centro de assistência técnica da marca.
                                                                        <br><br>9.5. Os acessórios abrangidos pela garantia, que se avariem, deverão ser remetidos, com o respetivo comprovativo de compra e/ou garantia, para a seguinte morada:
                                                                        FunkyNotes
                                                                        Faculdade de Ciências - DI
                                                                        Se o Utilizador optar por outras formas de devolução, os respetivos custos com portes de envio serão da sua responsabilidade. O Utilizador deve solicitar sempre o Talão dos CTT que comprova o envio da encomenda.
                                                                        <br><br>9.6. Se o equipamento se avariar e esta avaria não se encontre abrangida pela garantia, o Utilizador poderá dirigir-se com o mesmo, e respetivo comprovativo de compra, a um centro de assistência técnica da marca.
                                                                        <br><br>10.	Propriedade Intelectual
                                                                        <br><br>10.1. A Loja é um site registado e o Serviço prestado pelo próprio site é da responsabilidade da FunkyNotes.
                                                                        <br><br>10.2. O Utilizador reconhece que o Serviço contém informação confidencial e está protegido pelos direitos de autor e conexos, propriedade industrial e demais legislação aplicável.
                                                                        <br><br>10.3. O Utilizador reconhece que qualquer conteúdo que conste na publicidade, destaque, promoção ou menção de qualquer patrocinador ou anunciante está protegido pelas leis relativas a direitos de autor e direitos conexos, pelas leis relativas a propriedade industrial e outras leis de proteção de propriedade, pelo que qualquer utilização desses conteúdos apenas poderá ocorrer ao abrigo de autorização expressa dos respetivos titulares.
                                                                        <br><br>10.4. O Utilizador compromete-se a respeitar na íntegra os direitos a que se refere o número anterior, designadamente abstendo-se de praticar quaisquer atos que possam violar a lei ou os referidos direitos, tais como a reprodução, a comercialização, a transmissão ou a colocação à disposição do público desses conteúdos ou quaisquer outros atos não autorizados que tenham por objeto os mesmos conteúdos.
                                                                        <br><br>11.Condições de Segurança do Serviço
                                                                        <br><br>11.1. O Utilizador compromete-se a observar todas as disposições legais aplicáveis, nomeadamente, a não praticar ou a fomentar a prática de atos ilícitos ou ofensivos dos bons costumes, tais como o envio indiscriminado de comunicações não solicitadas (spamming) em violação do disposto na legislação aplicável ao tratamento de dados pessoais e às comunicações publicitárias através de aparelhos de chamada automática, devendo ainda observar as regras de utilização do Serviço, sob pena de a FunkyNotes suspender ou desativar o Serviço nos termos previstos no ponto 14.
                                                                        <br><br>11.2. O Utilizador expressamente reconhece e aceita que a Rede IP constitui uma rede pública de comunicações eletrónicas suscetível de utilização por vários utilizadores, e como tal, sujeitas a sobrecargas informáticas, pelo que a FunkyNotes não garante a prestação do Serviço sem interrupções, perda de informação ou atrasos.
                                                                        <br><br>11.3. A FunkyNotes não garante igualmente a prestação do Serviço em situações de sobrecarga imprevisível dos sistemas em que o mesmo se suporta ou de força maior (situações de natureza extraordinária ou imprevisível, exteriores à FunkyNotes e que pela mesma não possam ser controladas).
                                                                        <br><br>11.4. Em caso de interrupção da prestação do Serviço por razões de sobrecarga imprevisível dos sistemas em que o mesmo se suporta, a FunkyNotes compromete-se a regularizar o seu funcionamento com a maior brevidade possível.
                                                                        <br><br>12.	Suspensão e desativação do Serviço Loja
                                                                        <br><br>12.1. Independentemente de qualquer comunicação prévia ou posterior, a FunkyNotes pode, em qualquer altura, e de acordo com o seu critério exclusivo, descontinuar a disponibilização do Serviço e ou parte do Serviço a um ou todos os Utilizadores.
                                                                        <br><br>12.2. A FunkyNotes reserva-se ainda o direito de suspender ou fazer cessar imediatamente o acesso ao Serviço, nos seguintes casos:
                                                                        <br><br>1.	a) Quando o Utilizador não observe as condições de utilização referidas no ponto 4 e outras referidas nas Condições Gerais;
                                                                        <br><br>2.	b) Quando a FunkyNotes cesse o acesso à Loja, mediante comunicação prévia com uma antecedência de 15 dias sobre a data de cessação.
                                                                        <br><br>12.3. A suspensão ou a cessação do Serviço pela FunkyNotes, nos termos dos números anteriores, não importa o direito do Utilizador ou terceiros a qualquer indemnização ou outra compensação, não podendo a FunkyNotes ser responsabilizada ou de alguma forma onerada, por qualquer consequência resultante da suspensão, anulação, cancelamento do Serviço.
                                                                        <br><br>12.4. Nas situações acima descritas, a FunkyNotes comunicará ao Utilizador, previamente por forma a que este possa, querendo, salvaguardar o conteúdo da sua área de visualização de encomendas no prazo de 3 (três) dias úteis a contar do envio do e-mail ou disponibilização da informação na página principal do Serviço.
                                                                        <br><br>13.Comunicações
                                                                        <br><br>13.1. Sem prejuízo de outras formas de comunicação previstas nas presentes Condições Gerais, as notificações efetuadas ao Utilizador que se relacionem com o Serviço, incluindo eventuais alterações às presentes Condições Gerais, poderão ser efetuadas para o endereço de correio eletrónico do Utilizador, por SMS ou contacto telefónico.
                                                                        <br><br>13.2. O Utilizador aceita receber toda e qualquer comunicação e/ou notificação relacionada com a Loja Online, para a morada, telefone de contacto e ou endereço de correio eletrónico (“e-mail”) indicados no processo de encomenda.
                                                                        Em qualquer momento, pode solicitar o não recebimento destas comunicações e/ou notificações através do Formulário de Contacto ou através da opção da opção “Não receber a Newsletter” inscrita em cada Newsletter.
                                                                        <br><br>14.	Configurações Técnicas
                                                                        <br><br>14.1. Sem prejuízo do disposto no número seguinte, a FunkyNotes poderá alterar o Serviço e/ou as condições técnicas de prestação do mesmo, bem como as respetivas regras de utilização, devendo divulgar ao Utilizador tais alterações com uma antecedência mínima de 15 (quinze) dias.
                                                                        <br><br>14.2. A versão em cada momento em vigor das presentes Condições Gerais e dos seus anexos encontra-se disponível no sítio eletrónico funkynotes.tk
                                                                        <br><br>15.Comunicações
                                                                        <br><br>15.1. Sempre que a FunkyNotes entenda necessário ou conveniente otimizar a experiência de navegação e/ou melhorar as condições de conectividade, a mesma poderá reformular remotamente as configurações de rede.
                                                                        <br><br>15.2. Sem prejuízo do disposto nos números seguintes, e atento o carácter inovador do Serviço e as evoluções tecnológicas a que poderá estar sujeito, a FunkyNotes poderá alterar as configurações técnicas do mesmo sempre que tal se revele conveniente para o adaptar a eventuais desenvolvimentos tecnológicos.
                                                                        <br><br>15.3. A FunkyNotes não garante no entanto ao Utilizador a realização de quaisquer upgrades ou melhorias no Serviço.
                                                                        <br><br>15.4. Algumas upgrades ou novas funcionalidades do Serviço poderão estar disponíveis apenas contra pagamento do Utilizador e/ou subscrição, pelo mesmo, de Condições Específicas de utilização.
                                                                        <br><br>16.	Reclamações
                                                                        <br><br>16.1. O Utilizador pode submeter quaisquer conflitos contratuais, aos mecanismos de arbitragem e mediação que se encontrem ou venham a ser legalmente constituídos, bem como reclamar junto da FunkyNotes de atos e omissões que violem as disposições legais aplicáveis à aquisição de bens.
                                                                        <br><br>16.2. A reclamação deverá ser apresentada no prazo máximo de 30 (trinta) dias, contados a partir do conhecimento dos factos pelo Utilizador, sendo registada nos sistemas de informação da FunkyNotes que deverá decidir a reclamação e notificar o interessado no prazo máximo de 30 (trinta) dias, a contar da data da sua receção.
                                                                        <br><br>17.	Lei Aplicável
                                                                        O Contrato rege-se pela lei portuguesa.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button id="btn-exit" type="buttom" class="btn btn-primary seg reglog" data-dismiss="modal">Fechar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <!-- Button -->
                                <div class="col-md-offset-3 col-md-9">
                                    <button id="btn-signup" type="submit" class="btn btn-primary seg reglog">Registo</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div></div>

        <!-- Footer -->
        <footer class="text-center">
            <div class="footer-above">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-md-12">
                            <img id="logo" src="logo_branco.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-below">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-md-12">Grupo 2 - PTI/PTR - FCUL Copyright &copy;</div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- /.container -->

        <!-- jQuery -->
        <script src="../js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

    </body>
</html>


