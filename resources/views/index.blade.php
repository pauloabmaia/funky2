<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png"  href="Imagem1.png" />

        <title>FunkyNotes</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Theme CSS -->
        <link href="css/freelancer.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/portfolio-item.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
      <!-- Navigation -->
      <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" role="navigation">
          <div class="container">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                  <button type="button" class="btn btn-default1 navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                      <span class="sr-only">FunkyNotesNav</span>
                      <span class="glyphicon glyphicon-menu-down"></span>
                  </button>
                  <a class="navbar-brand" href="index">FunkyNotes</a>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <!-- dropdown -->
                <ul class="navbar-form navbar-left">
                  <div class="dropdown">
                    <button class="btn btn-default1 dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      <li class="opcao"><a href="espetaculos">Espectáculos</a></li>
                      <li class="opcao"><a href="cinema">LiveNow</a></li>
                      <li class="opcao"><a href="musicas">Música</a></li>
                      <li class="opcao"><a href="videos">Vídeos</a></li>
                    </ul>
                  </div>
                </ul>

                <div id="terminariniciar">
                    <ul class="nav navbar-nav navbar-right nav-main" id="reglogbar">
                          <?php
                          if (isset($_COOKIE['login']) and $_COOKIE['login'] == 'true'){
                          ?>
                            <ul style="margin-top:7px; " class="navbar-nav navbar-right" id="reglogbar">
                              <li>
                                <div class="dropdown">
                                  <button type="button" class="btn btn-default1 dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                  </button>
                                  <ul class="dropdown-menu" role="menu">
                                    <li class="opcao"><a href="divulgacao">Adicionar Espetáculos</a></li>
                                    <li class="opcao"><a href="upload">Upload Musicas/Videos</a></li>
                                  </ul>
                                </div>
                              </li>
                            <li>
                              <button type="button" class="btn btn-link dropdown-toggle " data-toggle="dropdown">
                                  <?php
                              $nick = $_COOKIE['nick'];
                              $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                              foreach ($users as $user) {
                                  $imagem = $user -> image;
                              }
                              echo"<span aria-hidden='true'><img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$imagem."' class='util img-circle' style='margin-top:-7px; margin-left:10px; '></span>";
                                      ?>
                                  </button>

                                  <ul class="dropdown-menu" role="menu">
                                      <?php
                              $nick = $_COOKIE['nick'];
                              $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                              foreach ($users as $user) {
                                  $dbid = $user -> id;
                              }
                              echo "<li><a href='artista?id=".$dbid."onClick='$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();''>Perfil</a></li>";
                                      ?>
                                      <li><a href="logout">Terminar Sessão</a></li>
                                  </ul>

                          </li>
                          <?php
                          }
                          else{
                          ?>
                          <li><a href='registo'>Registar</a></li>";
                          <li><a href='entrar'>Entrar</a></li>
                          <?php
                          }
                          ?>
                            </ul>
                      </ul>
                  </div>

                  <form class="navbar-form navbar-left" action="search" method="get" role="form">
                      <!-- <div id="custom-search-input"> -->
                          <div class="input-group">
                              <input type="text" class="form-control" placeholder="Pesquisa" name="pesquisa"/>
                              <div class="input-group-btn">
                                  <button class="btn btn-default1" type="button">
                                      <a href="#" onclick="$(this).closest('form').submit()"><i  class="glyphicon glyphicon-search"></i></a>
                                  </button>
                              </div>
                          </div>
                  <!--    </div> -->
                  </form>
              </div>
          </div>
      </nav>

        <!-- Portfolio Grid Section -->
        <section id="portfolio">
            <div class="container">
		<a href="musicas" id ="partista"><h3 class="text-center">Músicas</h3></a>
		<hr class="star-primary">
                <div class="row" style='text-align:center;'>
                    <?php
                    $musicas = DB::table('Musica')->select('*')->orderby('id', 'dsc')->take(8)->get();
                    foreach ($musicas as $musica) {
                        $dbnome = $musica -> nomemusica;
                        $dbid = $musica -> id;
                        $dbimage = $musica -> image;
                        $dbdescricao = $musica -> descricao;
                        echo "<div class='col-sm-3 portfolio-item'>
						<a href='musi?id=".$dbid."' class='portfolio-link' data-toggle='modal'>
							<div class='caption'>
								<div class='caption-content'>
									<i class='glyphicon glyphicon-play'></i>
									<br>
									<i class='fa fa-search-plus fa-3x'>".$dbnome."</i>
								</div>
							</div>
							<img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/musics/images/".$dbimage."' style='text-align:center;'  class='img-responsive'  alt='Cabin'>
						</a>
					</div>";
                    }
                    ?>
                </div>
            </div>

        </section>

        <!-- About Section -->
        <section class="success" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>Espetáculos</h2>
                        <hr class="star-primary">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 text-center">
                        <br>
                    </div>


                    <div class="col-lg-8 col-lg-offset-2 text-center">
                        <a href="#">
                            <a href='espetaculos'><button type="button" class="btn btn-danger btn-lg">Ver</button></a>
                            <br>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
        <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
            <a class="btn btn-primary" href="#page-top">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>


        <!-- Page Content -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1></h1>
                </div>
            </div>
        </div>

        <!-- About Section -->
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>Novidades da Semana</h2>
                        <hr class="star-primary">
                    </div>
                </div>


                <div class="row">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <!--<li data-target="#carousel-example-generic" data-slide-to="2"></li>-->
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="live.jpg" class="img-responsive" alt="...">
                                    <div class="carousel-caption">
                                        <h2>FunkyNotes Live</h2>
                                        <br>
                                        <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                                            <button type="button" class="btn btn-info btn-lg">Saber Mais</button>
                                        </a>
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="tex.png" class="img-responsive" alt="...">
                                    <div class="carousel-caption">
                                        <h2>Músicas</h2>
                                        <br>
                                        <p>A página de procura das suas músicas preferida, fácil, rápida e eficaz! Onde pode encontrar todas as músicas dos seus artistas favoritos, fazer o download das mesmas, assim como escutar.</p>
                                        <br>
                                        <a href="musicas">
                                            <button type="button" class="btn btn-primary btn-lg">Músicas</button>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Portfolio Modals -->
        <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <h2>FunkyNotes Live</h2>
                                <hr class="star-primary">
                                <!--<img src="img/portfolio/cabin.png" class="img-responsive img-centered" alt="">-->
                                <p>O FunkyNotes Live é uma funcionalidade nova, que permite saber que Espetáculos estão a decorrer neste momento!</p>
                                <a href="cinema"><button type="button" class="btn btn-danger"><i class="fa fa-times"></i>Live Now</button></a>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>Fechar</button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <!--<script src="vendor/jquery/jquery.min.js"></script>


<!-- Contact Form JavaScript -->
        <!--<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>

<!-- Theme JavaScript -->
        <!--<script src="js/freelancer.min.js"></script>


<!-- About Section -->
        <section class="successi" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>Quem Somos</h2>
                        <hr class="star-primary">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 text-center">
                        <p>O FunkyNotes permite que você oiça as Músicas preferidas dos seus artistas, ou que você seja um artista! Para isso basta criar uma conta connosco. Para além destas funcionalidades permite também assistir a Espetáculos e Vídeos </p>
                        <!--<img id="quemsomos" src="logo.png">-->
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer -->
        <footer class="text-center">
            <div class="footer-above">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-md-12">
                            <img id="logo" src="logo_branco.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-below">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-md-12">Grupo 2 - PTI/PTR - FCUL Copyright &copy;</div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- /.container -->

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    </body>
</html>


