<?php
	$id_bilhete = $_GET['id'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/png"  href="Imagem1.png" />
    <title>FunkyNotes</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Theme CSS -->
    <link href="css/freelancer.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/portfolio-item.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="btn btn-default1 navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">FunkyNotesNav</span>
                        <span class="glyphicon glyphicon-menu-down"></span>
                    </button>
                    <a class="navbar-brand" href="index">FunkyNotes</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <!-- dropdown -->
                  <ul class="navbar-form navbar-left">
                    <div class="dropdown">
                      <button class="btn btn-default1 dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li class="opcao"><a href="espetaculos">Espectáculos</a></li>
                        <li class="opcao"><a href="cinema">LiveNow</a></li>
                        <li class="opcao"><a href="musicas">Música</a></li>
                        <li class="opcao"><a href="videos">Vídeos</a></li>
                      </ul>
                    </div>
                  </ul>

                  <div id="terminariniciar">
                      <ul class="nav navbar-nav navbar-right nav-main" id="reglogbar">
                            <?php
                            if (isset($_COOKIE['login']) and $_COOKIE['login'] == 'true'){
                            ?>
                              <ul style="margin-top:7px; " class="navbar-nav navbar-right" id="reglogbar">
                                <li>
                                  <div class="dropdown">
                                    <button type="button" class="btn btn-default1 dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                      <li class="opcao"><a href="divulgacao">Adicionar Espetáculos</a></li>
                                      <li class="opcao"><a href="upload">Upload Musicas/Videos</a></li>
                                    </ul>
                                  </div>
                                </li>
                              <li>
                                <button type="button" class="btn btn-link dropdown-toggle " data-toggle="dropdown">
                                    <?php
                                $nick = $_COOKIE['nick'];
                                $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                foreach ($users as $user) {
                                    $imagem = $user -> image;
                                }
                                echo"<span aria-hidden='true'><img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$imagem."' class='util img-circle' style='margin-top:-7px; margin-left:10px; '></span>";
                                        ?>
                                    </button>

                                    <ul class="dropdown-menu" role="menu">
                                        <?php
                                $nick = $_COOKIE['nick'];
                                $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                foreach ($users as $user) {
                                    $dbid = $user -> id;
                                }
                                echo "<li><a href='artista?id=".$dbid."onClick='$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();''>Perfil</a></li>";
                                        ?>
                                        <li><a href="logout">Terminar Sessão</a></li>
                                    </ul>

                            </li>
                            <?php
                            }
                            else{
                            ?>
                            <script type="text/javascript">
                              window.location.href = "entrar";
                            </script>
                            <!--<li><a href='registo' onClick="$('#indexbox').hide(); $('#signupbox').show(); $('#reglogbar').hide(); $('#livenow').hide();">Registar</a></li>
                            <li><a href='entrar' onClick="$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();">Entrar</a></li>-->
                            <?php
                          }
                          ?>
                            </ul>
                      </ul>
                  </div>

                  <form class="navbar-form navbar-left" action="search" method="get" role="form">
                      <!-- <div id="custom-search-input"> -->
                          <div class="input-group">
                              <input type="text" class="form-control" placeholder="Pesquisa" name="pesquisa"/>
                              <div class="input-group-btn">
                                  <button class="btn btn-default1" type="button">
                                      <a href="#" onclick="$(this).closest('form').submit()"><i  class="glyphicon glyphicon-search"></i></a>
                                  </button>
                              </div>
                          </div>
                  <!--    </div> -->
                  </form>
              </div>
          </div>
      </nav>

    <!-- Portfolio Grid Section -->
    <div class="container">
         <div class="row">
            <div class="col-md-2">
            </div>

            <div class="col-md-8">
                <div class="bilhete">
                <section id="portfolio">
                    <table class="table-responsive" >
                            <table class="TableCol">
                                <tr>
                                    <td class="TableLeft">
                                        <?php
                                            $bilhete = DB::table('Bilhetes')->where('id', $id_bilhete)->select('*')->get();
                                            foreach ($bilhete as $chave) {
                                                $preco = $chave -> preco;
                                                $lugar = $chave -> lugar;
                                                $id_espetaculo = $chave -> id_espetaculo;
                                                $id_comprador = $chave -> id_comprador;
                                            }
                                            if($id_comprador != $dbid){
                                              echo "<script type='text/javascript'>
                                                window.location.href = 'index';
                                              </script>";
                                            }
                                            $espetaculo = DB::table('Espetaculos')->where('id', $id_espetaculo)->select('*')->get();
                                            foreach ($espetaculo as $cena) {
                                                $qtdbilhetes = $cena -> qtdBilhetes;
                                                $local = $cena -> local;
                                                $nome = $cena -> nome;
                                                $descricao = $cena -> descricao;
                                                $datainicio = $cena -> datainicio;
                                                $datafim = $cena -> datafim;
						$dbsala = $cena -> tamanhoSala;
						$linhascolunas = explode('x', $dbsala);
						$linhas = (int)$linhascolunas[0];
						$colunas = (int)$linhascolunas[1];
                                            }
                                            $espetaculo = DB::table('UtilizadoresRegistados')->where('id', $id_comprador)->select('*')->get();
                                            foreach ($espetaculo as $cenas) {
                                                $nomeComprador = $cenas -> nome;
                                            }
                                        ?>
                                        <table>
                                            <tr>
                                                <td><h1 id="nome"><?php echo $nome; ?></h1></td>
                                            </tr>

                                            <tr>
                                                <td><h3 id="local"><?php echo $local; ?></h3></td>
                                            </tr>

                                            <tr>
                                                <td><?php echo $nomeComprador; ?></td>
                                                <!--<td class="codigo">123456789</td>-->
                                                <td class="codigo">ID Bilhete: <?php echo $id_bilhete; ?></td>
                                            </tr>

                                            <tr>
                                                <td>Data inicio: <span id="data"><?php echo $datainicio; ?></span></td>
                                            </tr>
					    
					    <tr>
						<td>Data fim: <span id="datafim"><?php echo $datafim; ?></span></td>
					    </tr>

                                            <tr>
                                                <?php
						if($colunas == 0 and $linhas == 0){
							echo "<td>Quantidade de Lugares comprados:".$lugar."</td>";
						} else{
							echo"<td>Lugar:".$lugar."</td>";
						}
						?>
                                            </tr>

                                            <tr>
						<?php
                                                if($colunas == 0 and $linhas == 0){
							$precofinal = (int)$lugar * (int)$preco; 
                                                        echo "<td>Preço: ".$precofinal."€</td>";
                                                } else{
                                                        echo"<td>Preço: ".$preco."€</td>";
                                                }
                                                ?>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                    </table>
                    <button type="buttom" class="btn-block btn btn-default fa fa-times" style="margin-top:4%;" data-toggle="modal" id="authorize-button"><h4>Adicionar ao Calendário</h4></button>
                </section>
                </div>
            </div>
            	<div class="col-md-2">
            	<button type="button" class="btn btn-primary" style="margin-top:70%;" onclick="goBack()"><i class="fa fa-times"></i>Voltar</button>
		</div>
        </div>
    </div>
	<script>
		function goBack(){
			window.history.back();
		}
	</script>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-12">
                        <img id="logo" src="logo_branco.png">
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-12">Grupo 2 - PTI/PTR - FCUL Copyright &copy;</div>
                </div>
            </div>
        </div>
    </footer>
    <!-- /.container -->
    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!--Google Calendar API-->
    <script text="text/javascript">
        // Client ID and API key from the Developer Console
        var CLIENT_ID = '629533455095-ukqgf7phjs2mvg0s2krqrkqdomnh5u9g.apps.googleusercontent.com';

          // Array of API discovery doc URLs for APIs used by the quickstart
          var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

          // Authorization scopes required by the API; multiple scopes can be
          // included, separated by spaces.
          var SCOPES = "https://www.googleapis.com/auth/calendar";
          var authorizeButton = document.getElementById('authorize-button');

          /**
           *  On load, called to load the auth2 library and API client library.
           */
          function handleClientLoad() {
            gapi.load('client:auth2', initClient);
          }
          /**
           *  Initializes the API client library and sets up sign-in state
           *  listeners.
           */
          function initClient() {
            gapi.client.init({
              discoveryDocs: DISCOVERY_DOCS,
              clientId: CLIENT_ID,
              scope: SCOPES
            }).then(function () {
              // Listen for sign-in state changes.
              gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

              // Handle the initial sign-in state.
            updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            authorizeButton.onclick = handleAuthClick;
            });
          }

          function updateSigninStatus(isSignedIn) {
            if (isSignedIn) {
              addEvents();
            }
          }
          /**
           *  Sign in the user upon button click.
           */
          function handleAuthClick(event) {
            gapi.auth2.getAuthInstance().signIn();
          }
          /**
           * Print the summary and start datetime/date of the next ten events in
           * the authorized user's calendar. If no events are found an
           * appropriate message is printed.
           */
          function addEvents() {
            //Exemplo a ir buscar os eventos de uma pessoa, caso nao esteja a dar exprimentar mudar adicionar nos SCOPES ao link ".readonly"
            /**gapi.client.calendar.events.list({
                'calendarId' : 'primary',
                'timeMin' : (new Date()).toISOString(),
                'showDeleted' : false,
                'singleEvents' : true,
                'maxResults' : 10,
                'orderBy' : 'startTime'
            }).then(function(response) {
                var events = response.result.items;
                for (var i = 0; i < events.length;  i++) {
                    var event = events[i];
                    if (event.summary == "Appointment") {
                        var request = gapi.client.calendar.events.delete({
                            "calendarId" : 'primary',
                            "eventId" : event.id
                        });
                        request.execute(function(resp) {
                            console.log(resp);
                        });
                    }
                }
            })**/
            var data = document.getElementById("data").innerHTML.split(' ');
            var hora = data[1];
            data = data[0];
            var datafinal = document.getElementById("datafim").innerHTML.split(' ');
	    var horafinal = datafinal[1];
	    datafinal = datafinal[0];
	    var resource = {
                  "summary": document.getElementById("nome").innerHTML,
                  "location": document.getElementById("local").innerHTML,
                  "start": {
                    "dateTime": data + "T" + hora + "-00:00"
                  },
                  "end": {
                    "dateTime": datafinal + "T" + horafinal + "+01:00"
              }

            };
            var request = gapi.client.calendar.events.insert({
              'calendarId': 'primary',
              'resource': resource
            });
            request.execute(function(resp) {

            });
        gapi.auth2.getAuthInstance().signOut();
          }

    </script>
    <script async defer src="https://apis.google.com/js/api.js"
      onload="this.onload=function(){};handleClientLoad()"
      onreadystatechange="if (this.readyState === 'complete') this.onload()">
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>

