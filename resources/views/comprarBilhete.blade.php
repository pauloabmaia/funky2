<?php
use Illuminate\Support\Facades\Input;
    	$nrslugares = $_GET['bilhetes'];
	$lugares = explode(",", $nrslugares);
    	$qtdlugares = count($lugares);
	$id_espetaculo = $_GET['id'];
	$espetaculos = DB::table('Espetaculos')->select('*')->where('id', $id_espetaculo)->get();
	foreach ($espetaculos as $espetaculo) {
		$dbnome = $espetaculo -> nome;
		$dbdescricao = $espetaculo -> descricao;
		$dblocal = $espetaculo -> local;
		$dbdatainicio = $espetaculo -> datainicio;
		$dbdatafim = $espetaculo -> datafim;
		$dbqtdBilhetes = $espetaculo -> qtdBilhetes;
		$dbpreco = $espetaculo -> preco;
		$dbimagem = $espetaculo -> imagem;
		$dbsala = $espetaculo -> tamanhoSala;
		$idArtista = $espetaculo -> idArtista;
		$linhascolunas = explode('x', $dbsala);
		$linhas = (int)$linhascolunas[0];
		$colunas = (int)$linhascolunas[1];
		$now = time();
		$datei = strtotime($dbdatainicio);
		$datef = strtotime($dbdatafim);
		if($datei <= $now and $datef <= $now) {
			?>
			<script type="text/javascript">
				window.history.back();
			</script>
			<?php
		}
	}

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<link rel="icon" type="image/png"  href="../imagem1.png" />
	<title>FunkyNotes</title>

	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- Theme CSS -->
	<link href="css/freelancer.css" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="css/portfolio-item.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>

  <!-- Navigation -->
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="btn btn-default1 navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">FunkyNotesNav</span>
                        <span class="glyphicon glyphicon-menu-down"></span>
                    </button>
                    <a class="navbar-brand" href="index">FunkyNotes</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <!-- dropdown -->
                  <ul class="navbar-form navbar-left">
                    <div class="dropdown">
                      <button class="btn btn-default1 dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li class="opcao"><a href="espetaculos">Espectáculos</a></li>
                        <li class="opcao"><a href="cinema">LiveNow</a></li>
                        <li class="opcao"><a href="musicas">Música</a></li>
                        <li class="opcao"><a href="videos">Vídeos</a></li>
                      </ul>
                    </div>
                  </ul>

                  <div id="terminariniciar">
                      <ul class="nav navbar-nav navbar-right nav-main" id="reglogbar">
                            <?php
                            if (isset($_COOKIE['login']) and $_COOKIE['login'] == 'true'){
                            ?>
                              <ul style="margin-top:7px; " class="navbar-nav navbar-right" id="reglogbar">
                                <li>
                                  <div class="dropdown">
                                    <button type="button" class="btn btn-default1 dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                      <li class="opcao"><a href="divulgacao">Adicionar Espetáculos</a></li>
                                      <li class="opcao"><a href="upload">Upload Musicas/Videos</a></li>
                                    </ul>
                                  </div>
                                </li>
                              <li>
                                <button type="button" class="btn btn-link dropdown-toggle " data-toggle="dropdown">
                                    <?php
                                $nick = $_COOKIE['nick'];
                                $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                foreach ($users as $user) {
                                    $imagem = $user -> image;
                                }
                                echo"<span aria-hidden='true'><img src='https://s3-eu-west-1.amazonaws.com/funkybucket1/profiles/".$imagem."' class='util img-circle' style='margin-top:-7px; margin-left:10px; '></span>";
                                        ?>
                                    </button>

                                    <ul class="dropdown-menu" role="menu">
                                        <?php
                                $nick = $_COOKIE['nick'];
                                $users = DB::table('UtilizadoresRegistados')->select('*')->where('nick', $nick)->get();
                                foreach ($users as $user) {
                                    $dbid = $user -> id;
                                }
                                echo "<li><a href='artista?id=".$dbid."onClick='$('#indexbox').hide(); $('#loginbox').show(); $('#reglogbar').hide(); $('#livenow').hide();''>Perfil</a></li>";
                                        ?>
                                        <li><a href="logout">Terminar Sessão</a></li>
                                    </ul>

                            </li>
                            <?php
                            }
                            else{
                            ?>
                            <li><a href='registo'>Registar</a></li>";
                            <li><a href='entrar'>Entrar</a></li>
                            <?php
                            }
                            ?>
                              </ul>
                        </ul>
                    </div>

                    <form class="navbar-form navbar-left" action="search" method="get" role="form">
                        <!-- <div id="custom-search-input"> -->
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Pesquisa" name="pesquisa"/>
                                <div class="input-group-btn">
                                    <button class="btn btn-default1" type="button">
                                        <a href="#" onclick="$(this).closest('form').submit()"><i  class="glyphicon glyphicon-search"></i></a>
                                    </button>
                                </div>
                            </div>
                    <!--    </div> -->
                    </form>
                </div>
            </div>
        </nav>

	<!--<div class="container">
		<div class="row">
				<div class="col-lg-12">
					<h2 class="text-center">ESCOLHER LUGAR</h2>
					<hr class="star-primary">
				</div>
			</div>
		<div class="row">
			<div class="col-md-3">

				<div>
					<?php
					 	/*$path = "https://s3-eu-west-1.amazonaws.com/funkybucket1/shows/".$dbimagem;
						//$path = "./storage/fotoespetaculo/".$dbnome.".jpg";
						echo "<img src='".$path."' class='img-responsive ci' alt='fotoperfil'>";*/
					?>
					<h4 class="text-center" id="nomepessoa"><?php //echo $dbnome; ?></h4>
				</div>
			</div>

		<div class="col-md-6" style="overflow: scroll;">
			<div id = "lugares">
				<div class="btn-group" data-toggle="buttons-checkbox">
					<table class = "lugares">
						<tr>
							<div>
								<hr class="palco">
							</div>
						</tr>
						<tr class="correction">
							<th></th>
							<?php
							/*if ($colunas == 0 and $linhas == 0){
								echo "<p>Quantidade de bilhetes</p>";
								echo "<input type='number' min='0' class='form-control' id='qttbilhetes' name='qttbilhetes' placeholder='Quantidade Bilhetes' required>";
							} else{
								for ($c = 1; $c <= $colunas; $c++) {
									echo "<th>".$c."</th>";
								}
								?>
							</tr>
							<?php
							$lugaresOcupados = array();
							$bilhetes = DB::table('Bilhetes')->select('*')->where('id_espetaculo', $id_espetaculo)->get();
							foreach ($bilhetes as $bilhete) {
								$lugar = $bilhete -> lugar;
								array_push($lugaresOcupados, $lugar);
							}
							$pcol = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
							"M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
							for ($l = 0; $l < $linhas; $l++) {
								echo "<tr>
									<th>".$pcol[$l]."</th>";
									for ($c = 1; $c <= $colunas; $c++) {
										$idlugar = $pcol[$l].$c;
										if (in_array($idlugar, $lugaresOcupados)){
											echo "<th><input type='checkbox' class = 'checkai' id= ".$idlugar." disabled></th>";
										}
										else{
											echo "<th><input type='checkbox' class = 'checkai' id= ".$idlugar." onclick='bilhetes(this.id)' ></th>";
										}
									}
								echo "</tr>";
							}
					echo "<h6>Simulação da sala de espectáculo.<br>Seleccione os lugares que deseja escolher.</h6>";
        }*/
				 ?>
				 </table>
				</div>
			</div>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-3">
			<?php //if (!($colunas == 0 and $linhas == 0)){
			//echo "<h3 align='center'>Lugares Selecionados</h3>";
  //  } ?>
				<div class='panel panel-default'>
					<div class='panel-body' id = 'lugar'>

								<div class="modal fade" id="lugar1" tabindex="-1" role="dialog"  aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title">Lugar Seleccionado</h4>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											</div>
											<div class="modal-body">
												<img src="sala2.jpg" alt="lugar">
											 </div>
										</div>
									</div>
								</div>
					</div>
				</div>
				<div class="comprar">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#comprar" onclick="valor()">comprar</button>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#comprarpaypal" onclick="valor()">Comprar com PayPal</button>

					<!--Pagamento PayPal -->
					<!--<div class="modal fade" id="comprarpaypal" tabindex="-1" role="dialog"  aria-hidden="true">

				<!--	<div class="modal-dialog modal-md" role="document">
						<!-- Modal content-->
						<!--<div class="modal-content">
				<!--		<form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="signupform" class="form-horizontal" role="form" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><span><i class="glyphicon glyphicon-lock"></i></span> Pagamento com PayPal</h4>
							</div>
							<input id="tickets" type="hidden" value="" name="bilhetes">
							<?php /*echo "<input type='hidden' value='".$id_espetaculo."' name='id_espetaculo'>"; ?>
							<?php echo "<input type='hidden' value='".$dbid."' name='id_comprador'>";*/ ?>
							<div class="modal-body">
								<div class="container">
									<div class="row">
										<div class="col-xs-5 col-md-6">
											<div class="panel panel-info">
											<br>
											<div class='form-control pa'>
												Total:-->
												<?php
												/*	if ($colunas == 0 and $linhas == 0){
														if(isset($_COOKIE['lugares'])){
															$qtt = $_COOKIE['lugares'];
															$precofinal = (int)$dbpreco * (int)$qtt;
														}
													}
													if(isset($_COOKIE['lugares'])){
														$lugares = $_COOKIE['lugares'];
														$precofinal = (int)$dbpreco * (int)$_COOKIE['lugares'];
														echo "<span class='amount' id='amount'>". $precofinal."€</span>";
													} else{
														$precofinal = 0;
													}*/
											?>
											<!--</div>
											<br>
												<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
													<input type="hidden" name="cmd" value="_s-xclick">
													<input type="hidden" name="hosted_button_id" value="5ANGZB32RV2ZY">
													<table>
														<tr><td><input type="hidden" name="on0" value="Resumo">Resumo</td></tr>
														<tr><td><select name="os0">
															<option value="x Bilhetes">x Bilhetes €<?php //echo (int)$precofinal; ?> EUR</option>
														</select></td></tr>
													</table>
													<!--<input type="hidden" name="option_amount1" value='<?php //$precofinal ?>'>-->
												<!--	<input type="hidden" name="currency_code" value="EUR">
													<input type="image" src="https://www.paypalobjects.com/pt_PT/PT/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - A forma mais fácil e segura de efetuar pagamentos online!">
													<img alt="" border="0" src="https://www.paypalobjects.com/pt_PT/i/scr/pixel.gif" width="1" height="1">
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						</div>
					</div>
        </div>-->

					<div id="comprar" >

					<div class="modal-dialog modal-md" role="document" >
						<!-- Modal content-->
						<div style="width:107%;" class="modal-content">
						<form action="comprar" method="post" id="signupform" class="form-horizontal" role="form" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="modal-header">
								<h4 class="modal-title"><span><i class="glyphicon glyphicon-lock"></i></span> Pagamento</h4>
							</div>
							<div class="modal-body">
								<div class="container">
									<div class="row">
										<!-- You can make it whatever width you want. I'm making it full width
										on <= small devices and 4/12 page width on >= medium devices -->
										<div class="col-xs-3 col-md-3 modal-dialog">
											<!--CREDIT CART PAYMENT-->
											<div class="panel form-horizontal panel-info">
												<div class="panel-heading">Detalhes do Pagamento</div>
												   <div class="panel-body">
													<input id='tickets' type='hidden' value='<?php echo $nrslugares; ?>' name='bilhetes'>
													<?php echo "<input type='hidden' value='".$id_espetaculo."' name='id_espetaculo'>"; ?>
													<?php echo "<input type='hidden' value='".$dbid."' name='id_comprador'>"; ?>
													<div class="form-group">
														<label for="cardNumber">Nome da Pessoa</label>
														<div class="input-group">
															<input type="text" class="form-control" id="cardNumber" placeholder="Nome da pessoa responsável pelo bilhete" required autofocus />
															<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12"><strong>Tipo de Cartão:</strong></div>
														<div class="col-md-12">
															<select id="CreditCardType" name="CreditCardType" class="form-control" name = "cartao">
																<option value="Visa">Visa</option>
																<option value="MasterCard">MasterCard</option>
																<option value="AmericanExpress">American Express</option>
																<option value="Discover">Discover</option>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label for="cardNumber">Número do Cartão</label>
														<div class="input-group">
															<input type="number" min="0" max="9999999999999999"  class="form-control" id="cardNumber" placeholder="16 dígitos" required autofocus />
															<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-7 col-md-7">
															<br>
															<div class="form-group">
																<label for="expityMonth">Data de Expiração</label>
                                                                <select name='expireMM' id='expityMonth' required>
                                                                    <option value=''>Mês</option>
                                                                    <option value='01'>janeiro</option>
                                                                    <option value='02'>fevereiro</option>
                                                                    <option value='03'>março</option>
                                                                    <option value='04'>abril</option>
                                                                    <option value='05'>maio</option>
                                                                    <option value='06'>junho</option>
                                                                    <option value='07'>julho</option>
                                                                    <option value='08'>agosto</option>
                                                                    <option value='09'>setembro</option>
                                                                    <option value='10'>outubro</option>
                                                                    <option value='11'>novembro</option>
                                                                    <option value='12'>dezembro</option>
                                                                </select> 
                                                                <select name='expireYY' id='expityYear' required>
                                                                    <option value=''>Ano</option>
                                                                    <option value='10'>2017</option>
                                                                    <option value='11'>2018</option>
                                                                    <option value='12'>2019</option>
                                                                    <option value='12'>2020</option>
                                                                </select> 
												</div>
												</div>
												<div class="col-xs-5 col-md-5 pull-right">
													<div class="form-group">
												<label for="cvCode">Código CVV</label>
								<input type="number" min="0" max="999"  class="form-control" id="cvCode" placeholder="3 dígitos" required />
													</div>
													</div>
													<div class='col-md-12 col-xs-12'><br></div>
													<div class='form-row col-xs-12'>
														<div class='col-md-12 col-xs-12'>
															<div class='form-control pa'>
																Total:
															<?php
															if ($colunas == 0 and $linhas == 0){
																$precofinal = (int)$dbpreco * (int)$nrslugares;
                                  											} else{
																 $precofinal = (int)$dbpreco * (int)$qtdlugares;
															 }
														echo"<span class='amount' id='amount'>".$precofinal."€</span>";
															 ?>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--CREDIT CART PAYMENT END-->
											</div>
										</div>
									</div>
								</div>
								<script type="text/javascript">
								 function goBack() {
									window.history.back();
								}
								</script>
								<div class="modal-footer">
								<button id="btn-signup" type="submit" class="btn btn-primary seg reglog" style="margin-left:3%;">Pagar</button>
								<buttom onclick="goBack()" type="submit" class="btn btn-primary seg reglog">Voltar</buttom>
								</div>
							</div>
						</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<!-- Footer -->
	<footer class="text-center">
		<div class="footer-above">
			<div class="container">
				<div class="row">
					<div class="footer-col col-md-12">
						<img id="logo" src="logo_branco.png">
					</div>
				</div>
			</div>
		</div>
		<div class="footer-below">
			<div class="container">
				<div class="row">
					<div class="footer-col col-md-12">Grupo 2 - PTI/PTR - FCUL Copyright &copy;</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- /.container -->
	<!-- jQuery -->
	<script src="js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>
</body>
</html>

