<?php
/*
|----------------------------------------------------------------------
| Web Routes
|----------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index', function () {
    return view('index');
});

Route::get('/entrar', function () {
    return view('entrar');
});

Route::get('/artista', function () {
    return view('artista');
});

Route::get('/espetaculo', function () {
    return view('espetaculo');
});

Route::get('/registo', function () {
    return view('registo');
});

Route::get('/divulgacao', function () {
    return view('divulgacao');
});

Route::get('/comprarBilhete', function () {
	return view('comprarBilhete');
});

Route::get('/meusbilhetes', function (){
    return view ('meusbilhetes');
});

Route::get('/espetaculos', function () {
    return view('espetaculos');
});

Route::get('/upload', function () {
    return view('upload');
});

Route::get('/musicas', function () {
    return view('musicas');
});

Route::get('/musi', function () {
    return view('musi');
});

Route::get('/videos', function () {
    return view('videos');
});

Route::get('/video', function () {
    return view('video');
});

Route::get('/cinema', function () {
    return view('cinema');
});

Route::get('/compra', function () {
    return view('compra');
});

Route::get('/ticket', function () {
    return view('ticket');
});

Route::get('/historico', function () {
    return view('historico');
});

Route::get('/logout', 'Auth\LoginController@logout');

Route::post('/login', 'Auth\LoginController@entrar');

Route::post('/regist', 'Auth\RegisterController@registar');

Route::post('/divulgar', 'Bilhetes\DivulgationController@divulgar');

Route::post('/uploadmusica', 'Uploads\UploadController@musica');

Route::post('/mensagem','Mensagens\MensagensController@mensagem');

Route::post('/uploadvideo', 'Uploads\UploadController@video');

Route::post('/comprar', 'Bilhetes\NewBilhete@newBilhete');

Route::get('/inserirmusica({musica})', function($musica) {
    /*echo "<script type='text/javascript'>
            document.getElementById('inserirmusica').innerHTML = '*/
           //';
       // </script>";
    //DOMDocument::getElementById('inserirmusica');
    /*if(is_string($element))$element= this->createElement($cenas);
    return */

    /*echo "<script>
            window.location.href = '../musicas';
          </script>";*/
});

Route::get('/artista/{id}', function($id){
    $artista = DB::table('UtilizadoresRegistados')->find($id);
    foreach ($artistas as $artista) {
        $dbnick = $artista -> nick;
    }
    echo "<script>
            window.location.href = '../artista?id=".$id."';
          </script>";
});

Route::get('/storage/users/{filename}', function ($filename){
    $path = storage_path('public/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/search', 'SearchController@search');

Route::get('/home', 'HomeController@index');
