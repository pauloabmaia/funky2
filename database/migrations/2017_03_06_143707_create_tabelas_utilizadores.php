<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelasUtilizadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('UtilizadoresRegistados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nick')->unique();
            $table->string('nome');
            $table->string('genero');
            $table->date('dataNascimento');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UtilizadoresRegistados');
    }
}
