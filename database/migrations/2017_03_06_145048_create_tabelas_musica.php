<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelasMusica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('Musica', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idArtista');
            $table->string('nome')->unique();
            $table->string('image');
            $table->string('descricao');
            $table->string('nomemusica');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Musica');
    }
}
