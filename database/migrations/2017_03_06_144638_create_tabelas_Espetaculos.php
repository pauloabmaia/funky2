<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelasEspetaculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('Espetaculos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->text('descricao');
            $table->text('local');
            $table->dateTime('datainicio');
            $table->dateTime('datafim');
            $table->integer('qtdBilhetes');
            $table->integer('preco');
            $table->integer('idArtista')->default(0);
            $table->string('tamanhoSala');
            $table->string('imagem')->default('NULL');
            $table->timestamp('dataagendamento')->useCurrent = true;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Espetaculos');
    }
}
